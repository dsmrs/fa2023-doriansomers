// Arduino Sender sketch

#include <Wire.h>

byte i2c_rcv;               // data received from I2C bus
int counter;

void setup(){
	Wire.begin(); // join I2C bus as the master
	Serial.begin(9600);
	// initialize global variables
	i2c_rcv = 0;
  counter = 0;
  Serial.println("Sender setup");	
}

void loop(){
	Wire.beginTransmission(0x08); // informs the bus that we will be sending data to slave device 8 (0x08)
	Wire.write(counter);        // send value_pot
	Wire.endTransmission();       // informs the bus and the slave device that we have finished sending data
  Serial.print("Sender sent: ");
  Serial.println(counter);
  counter++;
	Wire.requestFrom(0x08, 1);    // request potentiometer position from slave 0x08
	if(Wire.available()) {	      // read response from slave 0x08
		i2c_rcv = Wire.read();
    Serial.print("Sender received: ");
    Serial.println(i2c_rcv);
	}
  delay(5000);
}