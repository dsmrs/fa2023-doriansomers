#include <Bounce2.h>

const int button_pin = 0;
int incomingByte; //red serial button
Bounce button_debouncer = Bounce(); 

void setup()
{
    Serial.begin(9600);
    pinMode(button_pin, INPUT);
    pinMode(LED_BUILTIN, OUTPUT);
    button_debouncer.attach(button_pin);
    button_debouncer.interval(5);
}

void loop()
{

    if (Serial.available()) 
    {   
    incomingByte = Serial.read();   
    switch(incomingByte)
    {
      case 'H':  
        digitalWrite(LED_BUILTIN,HIGH);
        break;
      case 'L': //your code
        digitalWrite(LED_BUILTIN,LOW);
        break;
      default:
        digitalWrite(LED_BUILTIN,LOW);
        break;
    }//end of switch()
    }//endof if 

  
    button_debouncer.update();
    if (button_debouncer.rose() == true)
    {
        Serial.println('H');
        digitalWrite(LED_BUILTIN, HIGH);

    }
    if (button_debouncer.fell() == true)
    {
        Serial.println('L');
        digitalWrite(LED_BUILTIN, LOW);

    }
}
