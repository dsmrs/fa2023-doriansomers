---
title: Week 9 - Output Devices
slug: week09
sidebar_position: 9
---

# Output Devices

**Group assignment:**
* Measure the power consumption of an output device.
* Document your work on the group work page and reflect on your individual page what you learned.

**Individual assignment:**
* Add an output device to a microcontroller board you've designed and program it to do something.



## Pilot a servomotor

On the board of the previous week, I've put a 3-pin connector with 5V, GND and a digital pin. Perfect to test how to pilot a small servomotor.

I have a [SM-S2309S](https://www.google.com/search?q=SM-S2309) micro servomotor. The pin I can use to control it is the `D0`.

![](./week09/kidcad1.jpg)

So I connected the servo-motor to my board and then try with this code from [this tutorial](https://docs.arduino.cc/learn/electronics/servo-motors)

```cpp
#include <Servo.h>

Servo myservo;  // create servo object to control a servo
// twelve servo objects can be created on most boards

int pos = 0;    // variable to store the servo position

void setup() {
  myservo.attach(D0);  // attaches the servo on pin D0 to the servo object
}

void loop() {
  for (pos = 0; pos <= 180; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
  for (pos = 180; pos >= 0; pos -= 1) { // goes from 180 degrees to 0 degrees
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
}
```

But actually, the `Servo.h` Library is not made for ESP32C3 architecture so it didn't compile. 

So I added [this Library for ESP32](https://github.com/RoboticsBrno/ServoESP32) and this time it compiles. But the servo doesn't move. So I checked the pin with a multimeter to look if I have 5V and the Ground. And I have it!

So maybe it's the `D0` signal ? So first I connected the Oscilloscope. I have a noisy signal. 

![](./week09/noisy-signal.jpg)

So I push [my test signal from the Wee6](./week06.md#oscilloscope) to see if the `D0` pin works properly. And it works! I have the signal on the oscilloscope. So maybe is in the code. I went on the repo of the Library if there were any settings I should have added. Then I saw [this issue](https://github.com/RoboticsBrno/ServoESP32/issues/20) and it looks like the _ESP32C3_ is not compatible yet with this library. I found [this post on Arduino's forum that tell us to use the [AnalogWrite Library for ESP32](https://github.com/Dlloydev/ESP32-ESP32S2-AnalogWrite).

So I rearrange [this code](https://wokwi.com/projects/356434791776045057) for my pin like this:

```cpp
#include <pwmWrite.h>

Pwm pwm = Pwm();

const int servoPin = D0;

void setup() {
}

void loop() {
  int position;  // position in degrees
  for (position = 0; position <= 180; position += 30) {
    pwm.writeServo(servoPin, position);
    delay(1000);
  }
}
```

Now I have a PWM signal.

import pwmSignal from './week09/pwm-signal.mp4';

<video controls width="80%">
  <source src={pwmSignal}/>
</video>

We can see on the Oscilloscope that the *period* of the signal is $20ms$ so the frequency is $\frac{1}{0,02sec} = 50Hz$. Moving the pin, I saw that the connection wasn't great enough, so I switch to an SG90 servo motor. But the pins are not in the same order so I used some cable to connect the servo correctly.

I add some buttonRead to turn the servo in one direction or to the other.

```cpp
#include <pwmWrite.h>

Pwm pwm = Pwm();

const int servoPin = D0;
const int buttonLeft = D1;
const int buttonRight = D2;

int position = 0;  // position in degrees


void setup() {
  pinMode(buttonLeft, INPUT);
  pinMode(buttonRight, INPUT);
  Serial.begin(9600);
}

void loop() {
  if (digitalRead(buttonLeft) == HIGH) {
    position +=30;
    Serial.print("left CW");
    Serial.println(position);
    pwm.writeServo(servoPin, position);


  }
  if (digitalRead(buttonRight) == HIGH) {
    position -=30;
    Serial.print("right CCW");
    Serial.println(position);
    pwm.writeServo(servoPin, position);


  }
  delay(1000);
}

```

Here is the video

import servoMotor from './week09/servomotor.mp4';

<video controls width="80%">
  <source src={servoMotor}/>
</video>


### Power consumption

I still have the UM34C power test. It can actually be connected by Bluetooth, and I found a [Desktop App](https://github.com/kolinger/rd-usb) to analyze the result on my computer.

So here are the results when the servomotor is plug-in.

![](./week09/rd-usb-servo.jpg)

I can see clearly when I push the button to move the servo. It can raise the current to $200mA$.


## OLED Screen through I²C

I bought some [small OLED display 0,96"](https://www.google.com/search?q=jmd0.96d-1). They connect with the *I2C* signal protocol.

On my board, a have 4 pins on the front to power and speak with an OLED display.

![](./week09/kidcad2.jpg)

Now let's connect it and display something. First I had to install [the library U$\infty g^2$](https://github.com/olikraus/u8g2) to help me communicate with the display.

So first I select in the Arduino IDE the [*GraphicTest* example](https://github.com/olikraus/u8g2/blob/master/sys/rt-thread/examples/full_buffer/GraphicsTest.cpp).

In the code, I uncommented this line to be used by the ESP32C3 as I read in [the wiki of the XIAO ESP32C3](https://wiki.seeedstudio.com/XIAO_ESP32C3_Pin_Multiplexing#software-setup)

```cpp
// U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock=*/ SCL, /* data=*/ SDA, /* reset=*/ U8X8_PIN_NONE);
```

And here we are, it's working!

import graphicTests from './week09/oled-graphictests.mp4';

<video controls width="80%">
  <source src={graphicTests}/>
</video>

I would love to try to show the Mosquito OS logo on the screen. So first I edit it to be 128x64 pixels.

![](./week09/mosquito_os_oled.png)

Then I had to convert it to [*XBM*](https://en.wikipedia.org/wiki/X_BitMap). I use an online converter to do it. Then I take [this example](https://github.com/olikraus/u8g2/blob/master/sys/arduino/u8g2_page_buffer/XBM/XBM.ino) and just change the XBM string by mine.

Fun fact, I try to take a picture, but the frequency of the screen is too low for my camera, so I only have a part of the picture.

![](./week09/part-mosquito-logo.JPG)

So here is a video:

import mosquitoLogo from './week09/mosquito-logo.mp4';

<video controls width="80%">
  <source src={mosquitoLogo}/>
</video>


## Stepper motor

I have a [28BYJ-48 stepper motor](https://www.google.com/search?q=28byj-48). It's a small stepper motor that does not draw a lot of current (~250mA) so I should be able to drive and power it with the board (but it's not recommended as it can put some noise signals on the board). I also have a [ULN2003 motor driver](https://www.google.com/search?q=uln2003+motor+driver).

I found [a nice tutorial with this stepper motor](https://lastminuteengineers.com/28byj48-stepper-motor-arduino-tutorial/). I'll need 4 PWM pins, so I'll use the ones that were on the board for the SPI signal. I send this code on the board :

```cpp
//Includes the Arduino Stepper Library
#include <Stepper.h>

// Defines the number of steps per rotation
const int stepsPerRevolution = 2038;

// Creates an instance of stepper class
// Pins entered in sequence IN1-IN3-IN2-IN4 for proper step sequence
Stepper myStepper = Stepper(stepsPerRevolution, D10, D9, D8, D7);

void setup() {
    // Nothing to do (Stepper Library sets pins as outputs)
}

void loop() {
	// Rotate CW slowly at 5 RPM
	myStepper.setSpeed(5);
	myStepper.step(stepsPerRevolution);
	delay(1000);
	
	// Rotate CCW quickly at 10 RPM
	myStepper.setSpeed(10);
	myStepper.step(-stepsPerRevolution);
	delay(1000);
}
```

The stepper motor vibrated but didn't turn. So I checked with the multimeter but all the tracks work correctly. So I check the signal with an oscilloscope.

As I can measure only two channels at a time.

Here are the motor Pins 1 and 2.

![](./week09/pin12.jpg)

And here are the pins 3 and 4.

![](./week09/pin34.jpg)

I can already see that it doesn't fit.

I recheck the wires, but they are connected in the right order.

I decided to change to another library: [AccelStepper](http://www.airspayce.com/mikem/arduino/AccelStepper/). And use the *AFMotor_ConstantSpeed* example. But I had the same problem.

I found this diagram about the right order to run the pins motors.

![](./week09/28byj-48-stepper-motor-specs.jpg)

*source [bluetin.io](https://www.bluetin.io/dc-motors/28byj-48-stepper-motor-driver-code/attachment/28byj-48-stepper-motor-specs/)*

So I put a really low speed with `myStepper.setSpeed(200);`. After that, it was so slow, that I can just have a look at the led turning red on the driver board and comparing with the image of the Phase Sequence.

import stepSlow from './week09/stepslow.mp4';

<video controls width="80%">
  <source src={stepSlow}/>
</video>

I can see that the `B` and `C` look reverse. And that doesn't make any sense... But when I switch the wires, it works. So I run this example from the library that accelerates and decelerates the motor.

```cpp
// Include the AccelStepper Library
#include <AccelStepper.h>

// Define step constant
#define MotorInterfaceType 4

// Creates an instance
// Pins entered in sequence IN1-IN3-IN2-IN4 for proper step sequence
AccelStepper myStepper(MotorInterfaceType, D10, D9, D8, D7);

void setup() {
  // set the maximum speed, acceleration factor,
  // initial speed and the target position
  myStepper.setMaxSpeed(1000.0);
  myStepper.setAcceleration(50.0);
  myStepper.setSpeed(200);
  myStepper.moveTo(2038);
}

void loop() {
  // Change direction once the motor reaches target position
  if (myStepper.distanceToGo() == 0) 
    myStepper.moveTo(-myStepper.currentPosition());

  // Move the motor one step
  myStepper.run();
}
```

Here is a video.


import stepMotor from './week09/stepmotor-final.mp4';

<video controls width="80%">
  <source src={stepMotor}/>
</video>

### Power consumption

Still with this code, I plug the UM34C to look at the current.

![](./week09/rd-usb-stepper-motor.jpg)

We can see the acceleration and deceleration of the motor. The motor consumes around 1,6 watts.

That's it for this week. I had some problems at my work, so I could do more this week. I'm a bit frustrated, I would love to play more with the display and the step motor. For the last one, I know next week of the Machine Design, I'll have the opportunity to play again with step motors.

## Files

Here are the files for the week

* [AFMotor_ConstantSpeed.ino](/files/week09/AFMotor_ConstantSpeed.ino)
* [check-pin-signal.ino](/files/week09/check-pin-signal.ino)
* [mosquito_os_oled.xbm](/files/week09/mosquito_os_oled.xbm)
* [MosquitoOLED.ino](/files/week09/MosquitoOLED.ino)
* [servomotor.ino](/files/week09/servomotor.ino)
* [steppermotor.ino](/files/week09/steppermotor.ino)

