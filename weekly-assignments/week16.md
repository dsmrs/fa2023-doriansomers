---
title: Week 16 - Applications and Implications
slug: week16
sidebar_position: 16
---

# Applications and Implications


## What will it do?

import Concept from '../final-project/_concept.mdx';

<Concept/>


## Who's done what beforehand?

import Inspirations from '../final-project/_inspirations.mdx';

<Inspirations/>


## What will you design?

### CAD

import trafficSignCAD from '../final-project/02-Traffic-sign/traffic-sign-cad.mp4';

<div className="steps">


<video controls width="100%">
  <source src={trafficSignCAD}/>
</video>

Here is a first look at the CAD file for the TrafficSign

![](../final-project/03-Wally/wally-module.jpg)

Here is a first idea about the case and all the components it needs to contain.

</div>

### Electronics

import GraphTrafficSign from '../final-project/02-Traffic-sign/_graph.mdx';
import GraphWally from '../final-project/03-Wally/_graph.mdx';

<div className="steps">

<GraphTrafficSign/>

**Traffic Sign board**<br/>A simple board that is able to received LoRa Signals, and show some information on a matrix LED.

<GraphWally/>

**Wally board**<br/>A board with an OLED screen and 2 buttons that can send LorA Signals.

</div>

## What materials and components will be used? Where will come from? How much will they cost?

### Traffic Sign

import BOMTrafficSign from '../final-project/02-Traffic-sign/BOM-traffic-sign.mdx';

<BOMTrafficSign/>


### Wally

import BOMWally from '../final-project/03-Wally/BOM-wally.mdx';

<BOMWally/>

## What parts and systems will be made?

1. Traffic Sign frame (made from aluminum and plywood)
2. 2 PCB for the Sign and the Wally board
3. A 3D Case

## What processes will be used?

### CNC milling machine 

1. 2 aluminum layers of the Traffic Sign
2. PCB for the *Wally Board*

### 3D print

1. Wally case

### Vinyl Cut

1. PCB for the LEDs Bike of the Traffic Sign
2. Reflective Vinyl for the Traffic Sign


### Laser Cut

1. Plywood cut for the Traffic Sign


## What questions need to be answered?

1. What's the maximal range of the LoRa Signal?
2. What will be the battery life for the Wally?
3. How much current does need the matrix LED?


## How will you organize yourself?

import Gantt from '../final-project/_gantt.mdx';

<Gantt/>


## How will it be evaluated?

* [x] Is all the layers of the Traffic sign fit together (kerf, bolts, etc.)?
* [x] Is the Matrix LEDs is able to show some text?
* [x] Is the Bike LEDs can light on?
* [x] Is the Wally board fit into its case?
* [x] Is the Wally board can send information through LoRa to the Traffic Sign?
* [ ] Is the Traffic Sign can receive LoRA data and show it on the matrix LEDs?
* [x] Is the OLED screen of the Wally Board can show some information?
* [x] Are the buttons of the Wally board can be pushed to send a LoRa Signal?
* [x] Is the Wally board can be powered by its battery?
