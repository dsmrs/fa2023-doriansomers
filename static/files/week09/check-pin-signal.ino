void setup() {
  pinMode(D0, OUTPUT); // set digital pin D1 as an output
}

void loop() {
  digitalWrite(D0, HIGH); // set pin D1 high
  delay(1000); // wait for 1 second
  digitalWrite(D0, LOW); // set pin D1 low
  delay(1000); // wait for 1 second
}
