# Dorian Somers - Fab Academy documentation

Here the repository of the Fab Academy 2023.

To understand where are the files:


```
dorian-somers/
├── weekly-assignements/
│   ├── img/                   # Images for the assignments
│   │   ├── week01/
│   │   ├── ...
│   │   └── week17/
│   │   │   └── *.jpg           
│   │   ├── week01.md          # Markdown files for the assignments
│   │   ├── ...
│   │   └── week17.md
├── final-project/
│   ├── img/
│   │   └── *.jpg              # Images for the final project
│   ├── project-proposal.md    # Markdown files for the final project
│   │   ├── ...
│   │   └── conclusion.md
├── blog/
│   ├── 2023-01-27-welcome.md  # Markdown files for the "Journal"
│   ├── ...
│   └── 2023-06-20-the-end.md
├── src/
│   └── pages/
│       └── about-me.md        # Other web pages
├── static/
│   └── img/
│       └── *.jpg              # Images for the other pages
├── .gitignore                 # Ignore files that don't need to be pushed
├── .gitlab-ci                 # Command to publish the website
├── README.md                  # Readme of the repo
├── agreement.md               # Student's agreement signed
├── ...                        # Some config files (lock, ...)
├── docusaurus.config.js       # Configuration file for Docusaurus (title, navbar, ...)
└── sidebars.js                # Configuration of the documentation's sidebar 
```

