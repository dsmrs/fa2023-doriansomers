---
sidebar_position: 1
---


# Components

Here is a map of the components linked together.

```mermaid
graph TD;
        od>Battery/Supraconductor]<== +24V DC ==> ci1((VESC))

        ci1 ==>|+24V DC|Regulator
        ci1 ==>|+24V DC|Relay
        ci1 ==>|+24V AC|di2{Direct <br/> Drive <br/> Motor}

        Relay ==>|+24V DC|ro1(Electromagntic Clutch)

        ro1(Electromagntic Clutch) <==>| friction-plate|di2

        Regulator -->|+5V DC|di{MCU Board}

        di{MCU <br/> Board} -.->|digitalWrite| Relay
        di -->|+5V <br/>DC| Relay
        di -.->|I2C|ci1
        di -.->|I2C|ci2((OLED <br/> Screen))
        di -->|+3.3V<br/> DC|ci2
        di -->|+3.3V<br/> DC|ci3
        di <-.-|I2C|ci3((Gyroscope & <br/> Accelerometer))
        di <-.->|SPI|ro2(Magnetometers)
        di <-.->|SPI|ro3(LoRa)
        di -->|+3.3V<br/> DC|ro2
        di -->|+3.3V<br/> DC|ro3
```