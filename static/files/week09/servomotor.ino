#include <pwmWrite.h>

Pwm pwm = Pwm();

const int servoPin = D0;
const int buttonLeft = D1;
const int buttonRight = D2;

int position = 0;  // position in degrees


void setup() {
  pinMode(buttonLeft, INPUT);
  pinMode(buttonRight, INPUT);
  Serial.begin(9600);
}

void loop() {
  if (digitalRead(buttonLeft) == HIGH) {
    position +=30;
    Serial.print("left CW");
    Serial.println(position);
    pwm.writeServo(servoPin, position);


  }
  if (digitalRead(buttonRight) == HIGH) {
    position -=30;
    Serial.print("right CCW");
    Serial.println(position);
    pwm.writeServo(servoPin, position);


  }
  delay(1000);
}
