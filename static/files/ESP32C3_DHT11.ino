#include "DHT.h"
#define DHTPIN D0     // Digital pin connected to the DHT sensor
 
// Here I define my DHT sensor (I have a DHT11)
#define DHTTYPE DHT11   // DHT 11
DHT dht(DHTPIN, DHTTYPE);
 
void setup()
{
  Serial.begin(9600);
  Serial.println("DHT11 test");
  dht.begin();
  delay(2000);
}
 
void loop()
{
  float h = dht.readHumidity();
  float t = dht.readTemperature();
 
  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t))
  {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.println("%");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.println("°C ");
  Serial.println("");
  delay(2000);
}
