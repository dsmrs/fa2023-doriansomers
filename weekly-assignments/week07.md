---
title: Week 7 - Computer-Controlled Machining
slug: week07
sidebar_position: 7
---

# Computer-Controlled Machining

**Group assignment:**

* Complete your lab's safety training
* Test runout, alignment, fixturing, speeds, feeds, materials and toolpaths for your machine
* Document your work to the group work page and reflect on your individual page what you learned

**Individual project**

* Make (design+mill+assemble) something big


## CNC Router

At the lab we have an [Arketype CNC Router](https://www.arketype-laser.fr/nos-machines/fraiseuse-cnc/), here are the specifications:

| Items                  | Value                              |
|------------------------|------------------------------------|
| Working area           | 1300 x 2500 x 100 mm               |
| Engraving speed        | 0-4000 mm/min                      |
| Structure              | Cast steel                         |
| Repositioning accuracy | <0,04/ 300mm                       |
| Collet                 | ER20                               |
| Spindles               | 3.2 kW 24000 Rpm                   |
| Power supply           | 220V ± 10%/50 Hz, 110V ± 10%/60 Hz |
| Dimensions             | 2000 x 3000 X x 1600 mm            |
| Vaccum Table           | 380V                               |

### Safety

When using the CNC router, we always wear that personal protective equipment: **glasses** and **ear protection**.

![](./week07/picture-epi.jpg)

If there is any emergency, I can stop the CNC with the emergency button.

![](./week07/picture_emergency.jpg)

There is also a **dust collector**, that can be run from the electrical panel, there I can also run the table vacuum.

![](./week07/picture-panel.jpg)

### Fixturing

For Fixturing, we usually use vises on an OSB sacrificial layer. But we can also use the vacuum table. For PCB making we use double tape.

To fix the board, I first a first corner. I go with the Endmill next to it. 

![](./week07/picture-first-corner.jpg)

Then I move on the Y axe till the end of the stock and put the stock on the endmill, so I'm sure to be aligned on the Y axe.

![](./week07/picture-second-corner.jpg)

### spindle and surface speed

Our endmill vendor has a really nice [documented page about the spindle, surface, feedrate speeds](https://www.cncfraises.fr/content/16-fraisage-definir-les-parametres-de-coupe), and even a spreadsheet to get the different parameters. In Fusion360 we can also get the parameters linked by the formulas.

So first the Spindle speed ($ss$). In the vendor documentation, the Surface speed ($sp$) for wood (Plywood, strong wood and MDF) is around $450-500m/min$. I plan to use endmills with a diameter ($d$) of $3-6-8mm$. So regarding the following equation linking those values:

$$
ss = \frac{1000*sp}{\pi * d}
$$

Here is a table with some $rpm$ for the 3 tools I'll use.

| End-mill diameter     | $3mm$ | $6mm$ | $8mm$ |
|----------------------|-------|-------|-------|
| Plywood ($500m/min$) | 53052 | 26526 | 19894 |
| MDF ($450m/min$)     | 47746 | 23873 | 17905 |

So for Plywood ($500m/min$) with a $3mm$ milling tool the Spindle speed is $53078 rpm$ and for a $8mm$ endmill in MDF ($450m/min$) we have an $ss = 17914$. The maximum spindle speed of our CNC router is $24000rpm$, so for the $3 - 6 mm$ tools, I will use this value and I'll go for $20000rpm$ for the $8mm$.

##  Cutting feedrate

To determine the feedrate ($fr$), I need to know the previous spindle speed ($ss$), the *feed per tooth* ($ft$) and the number of teeth/flutes ($n$). Here the equation:

$$
fr = ss * ft * n
$$

Here is a table for 3 different tools ($3mm$ *2-3 flutes* and $6-8mm$ *2 flutes*) and the data given but the vendor.

| Endmill diameter | $3mm$          | $6mm$ (2 teeth) | $8mm$ (2 teeth) |
|-----------------|----------------|-----------------|-----------------|
| Plywood         | 2520 (3 teeth) | 4320            | 4000            |
| MDF             | 4800 (2 teeth) | 14400           | 16000           |

As the CNC can't go faster than $4000 mm/min$, and actually we try to go even at a lower speed (maximum $3000 mm/min$), we probably should buy an endmill with only one flute.

### Runout and alignment test

To check the runout and the alignment of the CNC, I'll generate a toolpath of a square and a circle. For that, I'll use the CAM software we use at the lab: [CamBam](http://www.cambam.info/).

I can draw directly in the software a square and a circle. I draw the circle and rectangle in the middle of the screen, then edit their parameters (diameter, width, height and positions) later in the parameters panel on the left.

![](./week07/cambam1.jpg)

Then I will create the first toolpath for the external paths, I selected them and click on *profile*.

![](./week07/cambam2.jpg)

Now I have a *Machining* folder. There I can create the stock of all the machinings (the stock can also be changed in *groups* of machinings). I did an offset of $20mm$ to have some room to fix the wood board on the CNC.

![](./week07/cambam3.jpg)

Then I can select the machining and choose the parameters I want. Here I will use a 2 flute $6mm$ flat endmill. So I'll use a cut feedrate of $3000 mm/min$. I like to set a Clearance plane of $6mm$, so I'm sure the tool will not hit the fixturing vises. I set the target to just a bit more than the sock thickness ($15,2mm$) so I'm sure it will cut through the sacrificial layer. Depth  For the **Depth increment**, The vendor said in the datasheet of the endmill that for *soft materials* like plywood/MDF, we can set the depth increment of the cutting equal to the diameter of the tool. As we need to cut $15,3mm$ and can set a depth increment to $\frac{15,3mm}{3} = 5,1 mm$ so I don't force too much on the tool doing the same number of increments I will have done with an increment of $6mm$.

For the tabs, I usually set them automatically with CamBam and move them where I want them (not in the corners). I like large triangles with a height of half of the material's thickness.

The cut feedrate and the spindle speed are set on the CNC router, not in the GCode, but I write the information so I can have an estimated toolpath duration.

![](./week07/cambam4.jpg)

I did the same of an inner circle, just chose the inside cutting option instead of the outside.

With the `Alt` key and the mouse, we can move in a 3D view to have a look at the toolpath. It's nice to be sure where the tabs are or if the toolpath I going straight.

![](./week07/cambam5.jpg)

I can now generate the Gcode, by selecting the machining folder (otherwise I'll generate only the Gcode for a specific toolpath, it can be useful when there is a different endmill for example).

![](./week07/cambam6.jpg)

Then I went to the CNC router and turn it on. On the Digital Signal Processing (DSP) of the CNC controller, it first asks to go HOME.

![](./week07/arketype1.jpg)
*source: a member of the fablab*

:::caution

Be careful there is nothing on the table before clicking on *go HOME*.

:::

I use the technic explained in the [Fixturing section](./week07.md#fixturing) to fix my stock.


After I put the endmill, I move the tool where I want my origin in X and Y. 

![](./week07/arketype2.jpg)
*source: a member of the fablab*

For the Z origin, I put a piece of paper and move it in Z axis carefully till the paper does not move anymore and set the Z origin.

Then in the DSP I click on `RUN/PAUSE` and chose `UDISK File`.

![](./week07/arketype3.jpg)
*source: a member of the fablab*

I select the `cnc-tests.nc` file on the list. 

![](./week07/picture-dsp-select.jpg)

Now I need to choose the cutting feedrate (call WorkSpeed on the DSP). I set it to $3000 mm/min$ (by clicking on `RUN/PAUSE` button and using the numeric keypad). For the Spindle speed, there is a scale of 8 steps so I can change the speed from $3000rpm$ to $3000rpm$. I can change this by clicking to `Z-` and `ON/OFF` in the same time in the DSP. When I'm ok with it, I turn on the dust extraction put my glasses on my head and click on `OK`. I like to put my hand close to the emergency stop until I find the first lines of the toolpath ok.

![](./week07/picture-dsp-start.jpg)

So here is the result. 

![](./week07/picture-test-board.jpg)

It's **perfectly square** =).

![](./week07/picture-test-square.jpg)

The outer circle is exactly at $100mm$ like I set on CamBam.

![](./week07/picture-outer-circle.jpg)

And the same for the inner circle. So there is **no runout on the CNC.**

![](./week07/picture-inner-circle.jpg)


## Finger joints tolerance

As I want to create some finger joints this week, I would love to test the tolerance, for a nice fitting wood joint.

For that, I want to give a try to the *Path Workbench* of FreeCAD.

### Design the joints

<div className="steps">

![](./week07/freecad1.jpg)

In FreeCAD I started a new sketch. The idea is just to test a fitting joint. So I first need a square with a square hole.

![](./week07/freecad2.jpg)

Then a tab to fit into.

![](./week07/freecad3.jpg)

I then create a pad from the sketches (like in week 2, a month ago already, time flies !).

![](./week07/freecad4.jpg)

Then in the *Path Workbench* I created a new *Job* and selected the first body to start with.

![](./week07/freecad5.jpg)

I set the stock as the model and select the point where I want my origin to be.


![](./week07/freecad6.jpg)

There, I also chose a name for my G-code output. By experience, I know that the *linuxcnc* Post-processor is doing a great job. But I should look for maybe a better processor for the Richauto A11 controller that's piloting the CNC.

</div>

:::caution

It's in that panel, that I chose the `.nc` extencsion for my toolpath file, as it's what my CNC Router need.

:::

<div className="steps">


![](./week07/freecad8.jpg)

A need also to create a *tool bits* that I'll use to machine the assignments of the week. I can then add it to the job and chose the cutting feedrate.

</div>

:::caution

By default, the unit of the speed is set to $mm/s$ and not $mm/min$ as we saw earlier.

:::


<div className="steps">

![](./week07/freecad9.jpg)

Now that I have a tool bit, it can be selected for the milling job.

![](./week07/freecad10.jpg)

Now I can create an _operation_ called "profile" that will cut the "contour" of the piece of wood.

![](./week07/freecad11.jpg)

I defined the final Depth and the step down of the toolpath.

![](./week07/freecad12.jpg)

I did it also for the inner rectangle.

![](./week07/freecad13.jpg)

The rectangle inside of the piece, I need to do some *dogbones* if I want my joint to fit together. For that, I used the *Dressup* function of FreeCAD.

![](./week07/freecad14.jpg)

There are different style, I chose the default one called "dogbone".


![](./week07/freecad15.jpg)

I create an extra **joint tolerance** for the *hole cut* of $0,05mm$ following [this answer](https://www.quora.com/When-you-are-routing-a-wood-round-box-lid-how-much-tolerance-should-you-build-into-it-to-assure-proper-and-snug-fit). For that is the profile toolpath (contour) we can add an *Extra Offset*

![](./week07/freecad16.jpg)

Then I did exactly the same for the other piece.

![](./week07/freecad17.jpg)

I can now post-process the toolpath to generate the Gcode.

![](./week07/freecad18.jpg)

Actually, I forget to create some *tabs* to hold the piece to the main board. So I use another *Dressup* called *Tag Dress-up*.

![](./week07/freecad19.jpg)

Now I can look at the final result by simulating the job. It's good for my, so let's send the G-code to the CNC.

![](./week07/picture-joint-board.jpg)

Here the result on the OSB.

![](./week07/picture-joint.jpg)

The joint fi perfectly, on the first try ! =D

</div>


## Order all the end-milling tools

I wanted to try the CAM on Fusion 360 with a board to store the Endmills of the fablab (it's a mess), but I didn't have enough time. So maybe later...

![](./week07/picture-endmill.jpg)

![](./week07/picture-endmill-ordered.jpg)

![](./week07/endmills.jpg)

**TODO**: Machining the bits storage and write comments
> **Note**: look like there is a post-processor for our Richauto A11 CNC Controller for Fusion360 https://cam.autodesk.com/hsmposts?p=richauto I need to try it.
> **Update** The post-processor works perfectly.

![](./week07/fusion-add-tool.jpg)


## Building a part of the Final project

So the idea for the week is to see if I can rebuild a trike prototype I have in my garage from numerical command machines.

<div className="steps">

![](./week07/rear-mosquito.jpg)

I'll start small, with the rear of the trike that holds the wheels.

![](./week07/axle1.jpg)

So first I resketch the *rear wheel support*. Changing it a bit because I want to have *fingers joints* to fix the joint-ball axe and a *T-Bolt joint* to fix the axle link between the two wheel supports/holders.


![](./week07/axle2.jpg)

I added some parameters, like the OSB_thickness or the tab_gap. And then create the *pad* from the sketch.

![](./week07/axle3.jpg)

Then I worked on the link between the two *rear wheel support*. So I created a new sketch and import some lines from the other sketch. Like the circle for the *T-Bolt* joint.


![](./week07/axle4.jpg)

I only draw the left side of it as I will mirror it.

![](./week07/axle5.jpg)

When the pad and the mirror is done. Then I can switch to the *Path Workbench*. So, as I did before, I create some new jobs.

![](./week07/axle6.jpg)

The most difficult part here was to arrange the pieces in one plan. So I played with the origin of the jobs, to rotate and move the toolpaths as they were on the same plane.

![](./week07/axle8.jpg)

For the *Ballpoint axle support", as they are not 90° from none of the XYZ Axis, I had to redefine the Z axis by selecting one Edge of the body. 

![](./week07/axle9.jpg)

I also added a *Drill* operation for the holes on the board.

![](./week07/axle10.jpg)

And activate the *Peck* function to remove wood ships.

![](./week07/axle11.jpg)

I could then simulate the job.

![](./week07/axle12.jpg)

Here we go, after some hours all the toolpath are aligned on one plane. To be honest, it's much quicker with a 2D drawing on CamBam... I need to watch more FreeCAD tutorials because this process is super slow...


</div>

Let's machine it!

import rearAxleBoard from './week07/video-rearAxle-board.mp4';

<video controls width="80%">
  <source src={rearAxleBoard}/>
</video>

At the end of the video, we can see the function _peck_ chosen in the *drill toolpath*.

<div className="steps">

![](./week07/picture-miss-tab.jpg)

I forget to put some *tabs* in one of the pieces. So it damaged the square. I had to re-cut one more piece of the ball-joint axe holder.

![](./week07/picture-saw.jpg)

I use a saw to remove some tabs as it is super quick.

![](./week07/picture-pieces-rearAxle.jpg)

Here are all the pieces.

![](./week07/picture-squarebolt1.jpg)

As I don't have a square bold for the *T-Bolt joint*, I decide to make a quickly and dirty one (I promise, next time I'll make it with the CNC =).


![](./week07/picture-squarebolt2.jpg)

</div>

And now it's assembled!

<div className="carousel-container">

![](./week07/picture-realAxle.jpg)
![](./week07/picture-t-bone.jpg)
![](./week07/picture-wheelSupp.jpg)

</div>


And now on the trike!

<div className="carousel-container">

![](./week07/disassemble_mosquito.jpg)
![](./week07/mosquito-osb1.jpg)
![](./week07/mosquito-osb2.jpg)
![](./week07/mosquito-osb3.jpg)
![](./week07/mosquito-osb4.jpg)

</div>

## Files

Here are the files for the week.

* [wSupp2.nc](/files/week07/wSupp2.nc)
* [alignment-runout-tests.cb](/files/week07/alignment-runout-tests.cb)
* [axle-link.nc](/files/week07/axle-link.nc)
* [ball0.nc](/files/week07/ball0.nc)
* [ball1.nc](/files/week07/ball1.nc)
* [ball2.nc](/files/week07/ball2.nc)
* [ball3.nc](/files/week07/ball3.nc)
* [cnc-tests.nc](/files/week07/cnc-tests.nc)
* [finger_joints.FCStd](/files/week07/finger_joints.FCStd)
* [joint1.nc](/files/week07/joint1.nc)
* [joint2.nc](/files/week07/joint2.nc)
* [joints-test.nc](/files/week07/joints-test.nc)
* [rear_axle.FCStd](/files/week07/rear_axle.FCStd)
* [wSupp.nc](/files/week07/wSupp.nc)
