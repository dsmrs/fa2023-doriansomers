---
sidebar_position: 1
---


# Final project proposal


Build a human-powered trike with electric assistance based on the [4 bars linkage rear steering developed by Olivier & Nicolas Chambon](http://www.mosquito-velomobiles.com/concept.htm).

## What would it look?

This tricycle, based on [the trike of the Mosquito Velomobile](https://mosquito-velomobiles.com/trike.htm), will be **a trike with two electric motor wheels at the back and one human-powered wheel at the front**. The steering is possible by the linkage between the rear wheels and the main body of the frame creating a "virtual pivot" above the driver.

![project proposal](img/project-proposal.jpg)

## What will it be made of?

### [Frame](./02-frame/index.md)

Mainly a mix of **CNC milled wooding pieces and metal ones**. Some standard link pieces will be used to link the pieces together. 

### [Assistance](./03-assistance/index.md)

The ~~propulsion~~ traction of the vehicle will be powered by the **brushless rear motors**. The motor will be linked to the front wheel by a chain and a clutch to activate or not the assistance.

It will be controlled by a ~~custom made~~ **Electronic speed controller** (ESC) FESC 6.6. This ESC is capable of ****Regenerative braking** making the trike capable to climb some small hills and recharge the capacitors downhill.

The ESC will be powered by ~~ultracapacitor cells~~ a Litium-ion battery of 24Volt and 24Ah. 

### [Electronics](./04-electronics/index.md)

As small board will be connected to the FESC 6.6 to contorl it (like different level of assitance), to activate the regenrative breaking, to get some information like:

* speed
* compass
* battery level
* GPS info (?)
* Activate a magnetic clutch (?)
* ...

This we be show on a small OLED display.

This board will be able to act as ~~electronic deferential system (when the trike turn both wheels will not be at the same speed)~~

## Who would use it?

This trike will be a **mid-range vehicle**. Capable to join small towns from 10 to 30 km away at an average speed of 30-40 km/h. It can be used for individual mobility and with a trailer you can use it to transport goods.


## Projet development

Here a Gantt for the final project.

```mermaid
%%{init: {'theme':'neutral'}}%%
gantt
    dateFormat  YYYY-MM-DD
    excludes    Tuesday, Wednesday, Thursday
    section Milestones
        Final presentation sign-ups :milestone, 2023-05-24, 0d
        CNC not operationnal :milestone, 2023-05-31, 0d
        project presentations :milestone, 2023-06-7, 0d
        cutoff local to global :milestone, 2023-06-21, 0d
        cutoff global to local :milestone, 2023-06-28, 0d
        cutoff final decisions :milestone, 2023-07-05, 0d
    section Documentation
        Final proposal              :milestone, 2023-05-24, 0d
        Draft presentation/video    :milestone, 2023-05-31, 0d
        Proposal                    :active, d1, 2023-05-01, 7d
        Draft video/slide                 :d2, after d1, 3d
        Final project Requirements (BOM, slide, video, ...)  :d3, after d2, 19d
    section Frame
        CAD         : f1, 2023-05-07, 6d
        CNC jobs    : f2, after f1, 4d
        Assembly    : f3, after f2, 4d
        Add Assistance :f4, after f3 a2, 2d
    section Assistance
        Run test on FESC (I2C)     :a1, 2023-05-12, 2d
        Motor-to-trike (CAO + CAM)      :a2, after f1, 3d
    section Electronics
        Choose components   :e1, after a1 , 1d
        Code the firmware   :e6, after e1, 9d
        Redesign the board  :e2, after e1, 3d
        Produce the board   :e3, after e2, 3d
        Design the case     :e4, after e2, 3d
        Mount everything on the trike, :e5, after f4, 2d
```