# Fairing

## Aerodynamic

<div className="steps">

![](./air-resistance.jpg)<br/>*source 2020, Bicycling Science, 4th Edition, David Gordon Wilson and Theodor Schmidt, MIT Press*

One of the major resistance in mobility is the air resistance. It's following this equation: $F_{A} = \frac{C_{D}*A*V^2}{2}$.<br/>Meaning the small surface $A$ the better

![](./power-speed.jpg)<br/>*source 2020, Bicycling Science, 4th Edition, David Gordon Wilson and Theodor Schmidt, MIT Press*

Here we can see that Human-Power vehicles (velomobile) are much better for that than regular bikes.

![](./aero1.jpg)

Illustrate with car

![](./aero2.jpg)

I used *Stable diffusion* to change it with a bike and a velomobile.

</div>

## Computed Aided Fairing

Laser-cutting cardboard glued together with fiberglass.

![](./bamboo-fairing.jpg)

*source [mosquito-velomobiles.com](https://mosquito-velomobiles.com/)*