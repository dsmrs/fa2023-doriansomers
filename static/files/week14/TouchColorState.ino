#include <Bounce2.h>

const int button_pin = D0;
int colorState = 0; //red serial button
Bounce button_debouncer = Bounce(); 

void setup()
{
    Serial.begin(9600);
    pinMode(button_pin, INPUT);
    pinMode(D3, OUTPUT);
    button_debouncer.attach(button_pin);
    button_debouncer.interval(5);
}

void loop()
{

    if (Serial.available()) 
    {   
      String state = Serial.readString();
      colorState = state.toInt();   
      toggleLED();
    }//endof if 

    
    button_debouncer.update();
    if (button_debouncer.rose() == true)
    {
      toggleLED();
      Serial.println(colorState);
    }
}

void toggleLED() {
    switch(colorState)
    {
      case 1:  
        digitalWrite(D3,HIGH);
        colorState = 0;
        break;
      case 0: //your code
        digitalWrite(D3,LOW);
        colorState = 1;
        break;
      default:
        digitalWrite(D3,LOW);
        break;
    }//end of switch()
}