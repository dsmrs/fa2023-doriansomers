---
title: Week 8 - Electronics Production
slug: week08
sidebar_position: 8
---



# Electronics Production

**Group assignment:**

* Characterize the design rules for your in-house PCB production process: document feeds, speeds, plunge rate, depth of cut (traces and outline) and tooling.
* Document your work to the group work page and reflect on your individual page what you learned

**Individual assignment:**

* Make and test the development board that you designed to interact and communicate with an embedded microcontroller

## How to mill PCB on a big CNC

The CNC milling machine we have at my lab is a big CNC, not a real fit to create PCBs. But it should be precise enough. So let's give it a try.

### Setup milling machine

First, our sacrificial layer is really old, it is not straight enough to mill PCB. So I created a Gcode to mill a pocket in the sacrificial layer of 3mm deep to me sure my OSB is perfectly flat.

![](./week08/cambam-pcb-sup.jpg)

Once it's done I fixed an MDF board in the pocket. I will use double-tap glue on the PCB.

![](./week08/CNC-mdf.jpg)

Then I fix the small leftover PCB board to mill a first test. I also cleaned the surface will a bit of alcohol.

![](./week08/PCB-on-mdf.jpg)

### Set up the Z origin

Now, I need to be able to set the origin. For X and Y it's easy. It's the same as the previous week. But the `Z` the paper technique doesn't work anymore as it's not precise enough. As I've learned at our local lecture there are 3 methods to set Z:

* multimeter + 0,001 mm step. When it's "beep" it's good.
* drill + sound/dust
* let fall the bit to the surface (/!\ can break the bit if the height is too high)

So first I try the last version. Carefully. But when I was tightening the collet, the bit wasn't at the top anymore. So I when for the drill method. My CNC can do smaller steps than $0,005mm$, so I will do it like that.

![](./week08/0-01-step-cnc.jpg)


import setZ from './week08/set-z.mp4';

<video controls width="80%">
  <source src={setZ}/>
</video>

The thing is because I drill on copper, I wear one more safety equipment, a mask, to not breathe the dust. (I also am careful to not touch the dust's copper).

![](./week08/PPE-mask.jpg)

It's difficult to hear or see something as I drill on the plane. So after some tries, I finally have a mark on the plate.

![](./week08/PCB-mark.jpg)

But to be honest I wasn't really happy with this method. So I try the last method with a multimeter.


![](./week08/set-multimeter.jpg)


### Lines test with Mods

So I try to test the "Mods" method to do a lines test. I try first the *program* *G-Code PCB* (open server program)

![](./week08/mods1.jpg)

I imported Neil's PNG and set the parameters. So first I thought about drilling through the copper layer. Its thickness is $35 \mu m$, so I choose $0.04mm$. ~~Because I'll use a *V-Bit*, to know the diameter of the tool I can use an Isosceles triangle calculator. So it's 0.014mm.~~ **Edit:** A *V-bit* is more of a trapeze (see the _FlatCAM_ section below).

![](./week08/triangle-calculator.jpg)
*source [keisan.casio.com](https://keisan.casio.com/exec/system/1273850202)*

So here are my first parameters.

![](./week08/mods2.jpg)

I run _calculate_, looked at the preview and get the G-code. I run it for a first try "in the air" on the CNC. 

I need to find the proper **cutting feedrate** ($fr$). As the diameter is so small, I already know I'll have to use the maximum spindle speed ($ss$): $24000rpm$. I can take the equation from last week $fr = ss * ft * n$. The *feed per tooth* ($ft$) on my datasheet is $0,01$ and it's one tooth ($n$). So $fr = 24000* 0,01 * 1 = 240 mm/min$. For security, I'll choose a $200 mm/min$ cutting feedrate.

And then I run it with the `Z` zero set.

![](./week08/first-linetest.jpg)

I can see that it didn't pass through the copper. And at some point, it stops the mill.

So first I thought that we Z wasn't good. So I try to reset it just next to the first fail. But I forget to set the super small step on the CNC, so I went too far on `Z` and broke the bit. :anguished:

![](./week08/broken-bit.jpg)

Hopefully, I have plenty of V-bit of 0.1mm. So I changed it, make sure it was on $0,01mm$ step mode and reset the Z. And try again the Gcode.

![](./week08/second-linetest.jpg)

Still not engraving, so it looks like the MDF board is not flat. So run my $3mm$ deep pocket on the MDF to be sure it is as flat as the OSB. And then I changed to a new copper plate. First putting a double tap on it. Be sure it starts a bit before the board.

![](./week08/doubletap-pcb1.jpg)

Then peel off the tap protection.

![](./week08/doubletap-pcb2.jpg)

And change the parameters for the default one on *Mods*.

![](./week08/mods3.jpg)

The preview on the Gcode looks better, and the depth of the cut is $0,1mm$ more than enough to cut the copper.

![](./week08/mods4.jpg)

So I try this on the board.

![](./week08/linetest-result.jpg)

The result doesn't look super great, but still, now the copper is cut. A wanted to try to cut the edge of the board. So I imported the *interior png*


![](./week08/mods5.jpg)

Then I remembered that Ahmed told us at the local lecture that we needed to change the size of the PNG and add a sort of offset to the raster. But as I already milled the lines is too late to do the operation. I will stop for now to use _Mods_ and try *FlatCAM*.

### Path tests with FlatCAM

I looked on the web, for other PCB CNC milling technics. I quickly found two software that can handle Gerbers files and turn them in Gcode for CNC: [CopperCAM](https://www.galaad.net/coppercam-eng.html) and [FlatCAM](http:![](./week08/flatcam.jpg)rg/). Even if the first one looked convenient, the second one is free and open source, so I decided to give it a try.

For me it was interesting, to explore this tool to have a better understanding of PCB production, starting with Gerbers and Drill files. I looked to [this tutorial to learn more about FlatCAM](https://www.youtube.com/watch?v=--Cb11heuHc).

#### Make some Gerbers

But first I need Gerbers files. To test I won't start with my PCB from Electronic Design week. Better to design just some tracks to test it. So go back to KidCAD.

I create a new project and open the *Schematic Editor*. I just connected some _pinHeader_ with 1 pin connected to the Ground to test the milling process of it.

![](./week08/kidcad1.jpg)

I created 3 pairs of it, run the *Footprint assignment tool* and then when on the *PCB Editor*. There is set 3 different track widths and clearances.

![](./week08/kidcad2.jpg)

I then draw the route between the pinhead. As close as possible to test the clearance too. I didn't find a way to assign the different *Net Classes* declared (I had already that problem on week 6). So I edit the width and clearance manually.

![](./week08/kidcad3.jpg)

As I overwrite the track width, the DRC won't work, but it doesn't matter. Let's create the Gerbers Files (*File > Fabrication Ouptus > Gerbers (.gbr)...*).

I selected only the *F.Cu* and *Edge.Cuts* layers.

![](./week08/kidcad4.jpg)

Then I create directly the Drill Files for the *pinHeader* holes.

![](./week08/kidcad5.jpg)

Now I can open those files in *FlatCAM*. So I added the Gerbers Files and the Drill Files (*Excellon*).

![](./week08/flatcam1.jpg)

First I need to set the Origin correctly (on the left-bottom of my PCB).

![](./week08/flatcam2.jpg)


Then select the *F_Cu* Gerber Properties. So first I'll rename it simply _F_Cu_. 

![](./week08/flatcam3.jpg)

Then I need to *Isolation Tool* but first I need to know the diameter of my tool. As I said before I use a V-Bit tool. I kind of a trapeze shape.

import VBitDimension from './week08/v-bit-dimension.svg';

<VBitDimension />

*source [modsproject.org](https://modsproject.org/)*

So the diameter change regarding the depth tool is in the material. In *FlatCAM* there is a calculator to get the diameter with the *tip diameter*, the angle of the V-Bit and the depth. As I want to go $0,1mm$ in, I have a $0,1mm$ tip *V-bit* so the "kerf" of the tool will be $0,1352mm$

![](./week08/flatcam4.jpg)

I decided to first try with the same parameters as *Mods*. 4 passes and an overlap of 50%. Don't forget to select *Combine* and then *Generate Geometry*.

![](./week08/flatcam5.jpg)

Then it opens the properties of this new geometry. There I can set the *Cut Z* Depth as $-0,1mm$, and a safe height in `Z`. Then Generate the tool path.

![](./week08/flatcam6.jpg)

We then see the toolpath on the screen. Next is to create the toolpath for the drills. Here I can directly, in *Drilling Tool* in the Excellon file properties, generate the GCode. I set the depth to $2mm$ to pass through the board.

![](./week08/flatcam7.jpg)

The last thing is to create the Gcode for the *Edge_Cuts*, in the properties, I go to *Cutout PCB*. There I can set a tool diameter, depth, and multip-depth. As I use double tap, I won't use any "gap" (= tabs). Once the geometry generates, I can then generate the Gcode. Like I did for the first Gerber.

![](./week08/flatcam8.jpg)

With the 3 toolpaths generate, I can save the Gcode in their properties.

![](./week08/flatcam9.jpg)

Here are the 3 tools I'll use.

![](./week08/picture-tools.jpg)


I then run it on the CNC.

![](./week08/picture-tracks-test.jpg)

The result is pretty bad. Only the largest tracks are still connected $0,8mm$. I measure the clearance between the *Ground* part and a track, I get ~$0,70mm$.

![](./week08/picture-tracks-measure.jpg)

And went I look in FlatCAM. I should have a $0,50mm$. So probably the tool I use is already a bit used. *Actual radius* $= \frac{0,7-0,5}{2} + \frac{0,1352}{2}= 0,168mm$, and so the diameter is more than $0,3mm$... That's a lot! I created some new tracks on *KidCAD* to try even thicker tracks.

**new parameters:**

* track 0,8 and clearance 0,6
* track 1 and clearance 0,6
* track 1 and clearance 0,8

![](./week08/kidcad6.jpg)

 I will still use this bit for a new trial. But this time with a $0,3353mm$ diameter for the path. Only 2 passes and an overlap of 40%.

![](./week08/flatcam11.jpg)

And the result is much better! So let's try to solder some pins. The *pinHeader* fit perfectly. But it raises a question. The "pass-through components" are much easier to solder from the bottom. But not the SMDs (Surface Mounted Devices). So, how can I do it?

![](./week08/thicker-track.jpg)

So first I try to solder just above the plastic holder.

![](./week08/picture-tracks-solder.jpg)

Then without it. But when I test with the multimeter if the pins were connected. I discovered that the double-layer PCB where connected from the bottom.

![](./week08/picture-tracks-current.jpg)

So I removed some copper at the bottom.

![](./week08/picture-tracks-bottom.jpg)

Now that I know that, I remove some copper in advance for the other holes with a drill.

![](./week08/picture-drill.jpg)

![](./week08/picture-solder-2.jpg)


![](./week08/picture-solder-3.jpg)

I also try to solder with the pins put from the bottom of the board.

![](./week08/picture-solder-bottom.jpg)

## Let's make my final project PCB

So let's reuse the PCB I designed in week 6. I will just edit it, with my learnings. So first I deleted all the tracks to replace them with my preferred *Net Classes* ($0,8mm$ of width and $0,6mm$ of clearance). To delete only the track, I select *Tracks* in the *Selection Filter*.

![](./week08/kidcad7.jpg)

Then I edit the *Net Classes* and start the routing. I also Change the gab/tab of the Ground *_Zone_, as I did on the last test. To assure a good connection to the ground.

![](./week08/kidcad8.jpg)

I'm a bit afraid to still be able to plug the USB-C in the ESP32C3 as there are some tracks in front of it. So I tried to reduce a bit the size of the board, and I had to remove one of the holes to fix the PCB. I could try to use the Trought Hole connection of the ESP32, but it would mean I need to reverse all the connections in *mirror*. 

So here is the new PCB.

![](./week08/kidcad9.jpg)

I imported the new Gerbers and Drill Files in *FlatCAM*. There I rotate 90° the board as my platform to mill is too thin on the *X-axis*.

![](./week08/flatcam12.jpg)

I use the same parameters from my last tracks test. Just I added one more passe for the isolation toolpath.

Here is the result, far from perfect, but it will work.

![](./week08/picture-milled.jpg)

So I did a bit of cleaning and sanding.

![](./week08/picture-clean.jpg)

To solder the ESP32C3 I used a tap to hold it for the first point.

![](./week08/picture-hold.jpg)

Then I solder all the others. And I added some solder on on pin of the SMD components.

![](./week08/picture-pads.jpg)

Then I started with the LEDs.

![](./week08/picture-leds.jpg)

I received all my Resistors in one envelope. So I had to find the ones I needed, they all have a number on them. To find the code for the right resistor I used [this website](https://kiloohm.info/smd4-resistor/1206).

Here are the ones I need:

| Ohms          | SMD  |
|---------------|------|
| $180\Omega$  | 1800 |
| $1k \Omega$  | 1001 |
| $10k \Omega$ | 1002 |

![](./week08/picture-resistors.jpg)

Then I solder them. Using the technic I saw during the Student's Bootcamp.

![](./week08/picture-res-soldered.jpg)

I added the pinHeaders and the button, and here we go the board is almost completed.

![](./week08/picture-finish-solder.jpg)

Almost, because like a saw, I made some mistake on the PCB. 

1) A missing track to connect a pin to the ground.
2) A missing track for the +3.3V to be connected to the Left side of the board.
3) a connection between a LED and its resistor.

![](./week08/picture-mistake.jpg)

Here is the final result:


import finalBoard from './week08/final-board.mp4';

<video controls width="80%">
  <source src={finalBoard}/>
</video>

## Files

Here are the files for the week.

* [final-project-board.kicad_pro (week6)](../static/files/week06/final-project-board/final-project-board.kicad_pro)


* [milling-test.kicad_pro](/files/week08/milling-test.kicad_pro)
* [milling-test.kicad_sch](/files/week08/milling-test.kicad_sch)
* [milling-test-Edge_Cuts.gbr](/files/week08/milling-test-Edge_Cuts.gbr)
* [milling-test-F_Cu.gbr](/files/week08/milling-test-F_Cu.gbr)
* [milling-test-NPTH.drl](/files/week08/milling-test-NPTH.drl)
* [milling-test-NPTH-drl_map.gbr](/files/week08/milling-test-NPTH-drl_map.gbr)
* [milling-test-PTH.drl](/files/week08/milling-test-PTH.drl)
* [milling-test-PTH-drl_map.gbr](/files/week08/milling-test-PTH-drl_map.gbr)
* [millling-test.FlatPrj](/files/week08/millling-test.FlatPrj)
* [milling-test.kicad_pcb](/files/week08/milling-test.kicad_pcb)
* [milling-test.kicad_prl](/files/week08/milling-test.kicad_prl)