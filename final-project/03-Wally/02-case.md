# Case

## Concept

<div className="steps">

![](wally-module.jpg)

Here is a first idea about the case and all the components it needs to contain.

</div>

## CAD

<div className="steps">

![](./case/display_dim.jpg)<br/>
*source [displaymodule.com](https://www.displaymodule.com/products/0-96-inch-oled-graphic-display-128x64-with-i2c)*

To design the case properly I had to know all the dimensions, so of course I [designed the board first](./electronics#design-the-board), but in the end it's back and forth between electronic design and the case design. I also need the dimensions of some components, especially the OLED display.

![](./case/esp32_dim.jpg)*source [sigmdel.ca](https://sigmdel.ca/michel/ha/xiao/seeeduino_xiao_01_en.html)

I also wanted to know the dimensions of the ESP32 board, like how much the USB TYPE-C plug intake from the board.

![](./case/wally1.jpg) 

Base on that I redesign the most important inner parts to be able the design the case around it. Like the OLED with the spacing of the pinout, the battery on the bottom, the left and right button or the LoRa antenna.

![](./case/wally2.jpg) 

Then I design a basic case. There is some support for the board. I let some hole for inserts, the antenna have its protection. And there are apertures for the buttons and the display.

![](./case/wally4.jpg) 

The idea behind Fablab is to not reinvent the wheel. So I found this handlebar support [from Gustavo Deabreu's final projet]https://fabacademy.org/2019/labs/barcelona/students/gustavo-deabreu/projects/__final_project/#design-files an Fab Academy alumni (2019). I have no purpose to commercialize this product so it complies [his CC-BY-C-SA licence](https://fabacademy.org/2019/labs/barcelona/students/gustavo-deabreu/projects/__final_project/#license). 

![](./case/wally3.jpg)

So based on the dimensions of the handlebar support, I design a connection to be fixed on the bottom of the case.

</div>

## 3D Print

<div className="steps">

![](./case/cura_case.jpg)

So let's print this.

![](./case/case2.jpg)

Golden is the way to go =) Just basic PLA. Of course, the end product should be more UV tolerant.

![](./case/case4.jpg)

Then I added some insert embedment nuts, so the case will be easily dismantlable. I just use a soldering iron to push them into the plastic.

![](./case/case5.jpg)

The PCB fits perfectly, but I made some mistakes with the placement of the holes. This will be fixed in the next iteration.


</div>

### Display dimenssion

<div className="steps">


![](./case/display2.jpg)

I added a nice detail, a place to put a small acrylic glass to protect the display.

![](./case/display3.jpg)

It fit perfectly.

![](./case/display1.jpg)

From the design, I realized that the pinhead female + male for the OLED will give me a high height for the case. So I decided to cut a part of it (both for the female and male end).

</div>

### Buttons

<div className="steps">

![](./case/button1.jpg)

 I try a first design to be able to reach the button of the board from the surface of the case. But ther were not fitting well.

![](./case/button3.jpg)

So I change the button for some were you can plug some caps.

![](./case/caps-pushbutton.jpg)*source [bobshop.co.za](https://www.bobshop.co.za/item/588174736/Round_Button_Cap_for_12mm_Tactile_Push_Switch_Silver.html)*

Those caps are too small, so I get their dimensions.

![](./case/button2.jpg)

And redesign caps to go and top of the caps ^^.

</div>


## Final assembly

<div className="steps">

![](./case/mistakes.jpg)

After some trial and errors, I finally get it.

![](./case/assembly1.jpg)

Here are all the parts ready to be assembled.

![](./case/assembly5.jpg)

Let's start by fixing the display to the top of the case.

![](./case/assembly2.jpg)

Then we can connect the battery. As I switch in last minute for an SMAD21 that doesn't have a battery charger system include, Ahmed, my instructor, gave me one [TP4056 Standalone linear Li-Ion Battery Charger](http://www.tp4056.com/). It was pretty straightforward to connect.

![](./case/assembly3.jpg)

Then we can close the case with its 4 screw on the top (maybe I should be them at the bottom on day).

![](./case/assembly6.jpg)

I do not forget to screw the handlebar support connector.

![](./case/assembly4.jpg)

And it's working on battery !

</div>

## Files

Here is the view of the Fusion 360 file for the case. You can [download it here](./files/Wally_case.f3d).


<iframe src="https://myhub.autodesk360.com/ue2d55e35/shares/public/SHd38bfQT1fb47330c99e9c1f6e997da89d8?mode=embed" width="100%" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>


Here are the other files for the case:

* [bottom_case.stl](./files/bottom_case.stl) 
* [caps.stl](./files/caps.stl) 
* [supp_handlebar.stl](./files/supp_handlebar.stl) 
* [top_case.stl](./files/top_case.stl) 
* [antenna_cover.stl](./files/antenna_cover.stl)