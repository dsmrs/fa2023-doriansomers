//Includes the Arduino Stepper Library
#include <Stepper.h>

// Defines the number of steps per rotation
const int stepsPerRevolution = 2048;

// Creates an instance of stepper class
// Pins entered in sequence IN1-IN3-IN2-IN4 for proper step sequence
Stepper myStepper = Stepper(stepsPerRevolution, D10, D9, D8, D7);

void setup() {
    // Nothing to do (Stepper Library sets pins as outputs)
      myStepper.setSpeed(200);

}

void loop() {
  // Rotate CW slowly at 5 RPM
  myStepper.step(stepsPerRevolution);
  delay(1000);
}
