---
title: Week 3 -  Computer-Controlled Cutting
slug: week03
sidebar_position: 3
---

import jointPlay from './week03/joint-play.mp4';
import scaleModel from './week03/scale-model-video.mp4';

# Computer-Controlled Cutting

* Linked to the group assignment page.
* Explained how you created your parametric design.
* Documented how you made your press-fit kit.
* Documented how you made something with the vinyl cutter.
* Included your original design files.
* Included hero shots of your results.

## Spiral Goals

* [x] Check the focus, power, speed, kerf, and joint clearance of the Arketype CO2 laser (**Assignment**)
* [x] Redraw the week mockup to design a laser cut scale model of the Final Project, including 2 different joints (**Assignment**)
* [x] Make the logo cut with the plotter and put it on the scale model (**Assignment**)
* [x] Cut the scale model with the laser and assemble it (**Assignment**)


## Laser CO2 parameters

As I'm doing the Fab Academy remotely, I had to do the group assignment myself on the machine of my lab.

*Update 12/03/2023*

At the lab we have [ArkeType](https://www.arketype-laser.fr/wp-content/uploads/2022/11/Gamme-JADE.pdf)), here are the specifications:

| Items       | Value|
|-------------|------------------------|
| Working Area| 90 cm x 60cm        |
| Power Laser | 100 Watts           |
| Engraving speed|  			0-50000 			mm/min 		    |
| Cutting speed  |  			8 			000 mm/min 		      |
| Raster engraving             |  			Maximum 			2500 DPI 		  |
|  			Precision 		 |  			<0,01 mm 		          |
|  			Taille 			mini des caractères 		 |  			Lettre 			1,0 x 1,0 mm 		  |
|  			Files formats 		|  			BMP, 			PLT, DST, DXF, AI, CDR 		        |
|  			Alimentation 		 |  			220V 			± 10%/50 Hz, 110V ± 10%/60 Hz 		 |
| Size        | 1400 x 1030 x 1050 mm  |

### Security

For the fumes of the laser, we have an [BOFA AD](https://bofainternational.com/en/find-products/extraction-systems), that we turn on when the laser runs.

![](./week03/BOFA.jpg)

Of course, there is also a *stop* button in case of emergency.

![](./week03/laser-emergency.jpg)

### Starting up

When we need to launch a job on the laser, we need to first turn on the laser (It automatically turns on the water cooling, but not the fume extractor). We can move the laser nozzle with the arrow from the X and TY axies. Usually we use the top-right corner of the drawing as the origin. That how I know where to put the nozzle on the material.

![](./week03/laser-op.jpg)

To set the auto-focus, we need to operate in the Z axe, by click on the `Z` on the dashboard. Now, I can move with the up and down arrows to move the working area. While my nozzle is on top of the material I want to cut, I can than click on `Datum` to set the focus, helped with the sensor button next to the nozzle.

![](./week03/laser-zdatum.jpg)

When it's set, on the computer connected with USB to the laser, in the [Lasercut 5.3 software](https://hpclaser.co.uk/lasercut5-3-software/) we can import or draw vectoriel design to cut or engrave. Once the settings are good, we just need to click on the *Download button* to send the laser operations to the machine. 

![](./week03/press_kit_lasercut.jpg)

Once is downloaded, we can click on 

### Focus

For the focus of the laser in my lab ([ArkeType](https://www.arketype-laser.fr/wp-content/uploads/2022/11/Gamme-JADE.pdf)), I used a "ramp" method. I put a piece of wood like a ramp starting at the real bottom of the laser nozzle. 

![ramp picture](./week03/ramp-laser.jpg)

Then I can burn a line from the top to the bottom of the ramp. 

![ramp laser](./week03/ramp-test.jpg)



After identifying the finest line, I measured the distance with the nozzle. 

![focus spot](./week03/focus-spot.jpg)

It appears to be perfect regarding the auto-focus already configured.

![focus distance](./week03/focus-distance.jpg)


### Power and speed

Next, I wanted to do a nice matrix of power/speed tests on 2 materials. As we don't run Lightburn as our laser software, but the one from the machine constructor, I need to redraw a test matrix from the start. the number of layers is limited, so I run it from 10 to 50% in power and 10 to 60 mm/s in speed. (I did it in French and English so the members of the Fablab can use it too).

![test cut](./week03/test-cut.svg)

Weirdly, the difference between the marked and the cut parts should have been diagonal, but instead, some parts stayed fixed. For the plywood it can be understood as some parts of the material can be stronger or softer, but for the cardboard is more *bizarre*.

![cut test](./week03/cut-test.jpg)


### Kerf 

For the kerf, I just draw some squares directly on the LaserCut software. Their width is equal to 20mm. [Here is the SVG file](./week03/test-kerf.svg). I set the power to $50 \%$ and the speed to $10 mm/s$ (not like the screenshot).

![kerf test](./week03/kerf-test.jpg)

Taking 7 pieces, I measured them, and instead of $140 mm$ as expected, I had $137,92 mm$.

![kerf measure](./week03/kerf.jpg)

If you subtract it with the designed length en divide by the number of lines you have the kerf(~diameter) of the laser

$$
\frac{140 mm - 137,92 mm}{8} = 0,26 mm
$$

###  Joint clearance

Now that we have our kerf, we can try different offsets to fill the gap of the kerf. To fit the kerf, the path needs to be at the kerf divide by two $0,26mm /2 = 0,13mm$

I draw a single finger joint and a circle fitting in a hole, [inspired by this](https://www.o2creative.co.nz/laser/lightburn_kerf_generator.php), but since Lightburn isn't compatible with our laser, I needed to redraw as I did for the cutting test. 

![test joint clearance](./week03/test-joint-clearance.svg)

I did 4 offsets from the kerf with steps of $0,01mm$.

![offset joints](./week03/joint-clearance.jpg)


All the finger joints were fitting perfectly, $0,15mm$ was the better actually. But for the circle in the hole from $0,13mm$, it's starting to not fit anymore.

![joint clearance picture](./week03/joint-clearance-test.jpg)

### Snap-fit joints

As I want to use some snap-fit joints for my scale model, I need to find the best parameters for it. So I created a new FreeCAD file for it. Compliant Joints are real science, that takes time to master. Here I'll just make some tests to find some joints that are working. 

![](./week03/snap-fit-joint.jpg)

*source: [SMlease Design](https://www.smlease.com/entries/plastic-design/how-to-design-snap-fit-joints-in-plastic-parts/)*

I draw 2 snap-fit joints with some arbitral measures taken to some literature. Some important measures are the deflection length (meaning the *nose*'s length of the snap) and the thickness of the snap (the finer,  the easier it will move, but the wackier it will be). I started will a thickness/deflexion of 3mm/1mm and 2mm/1,5mm.

![](./week03/snap-tests.jpg)

I exported the sketch (select the sketches then Files > export).

![](./week03/freecad-export-dxf.jpg)

Then import the DXF files into the LaserCut software. I did an offset of $0,13mm$ as seen in the [Joint clearance](./week03.md#joint-clearance) section. Then cut some first pieces.

![](./week03/snaps-picture.jpg)

It didn't go well. The 3mm/1mm broke the end-piece joint and on the 2mm/1,5mm one of the snap broke.

![](./week03/snaps-broken.jpg)

So I did another try by changing the `snap_deflection` variable to $0,5mm$ and the `snap_entrance_angle` to $25°$. It worked wonderfully, but the joints were moving.

<video controls width="80%">
  <source src={jointPlay}/>
</video>

So I reduce the height of the snap from $5mm$ to $4,5mm$, and that's a perfect fit! The snap-fit joint with a thickness of 3mm is a bit harder to put in but looks stronger, so that's the one I'll choose for the scale model.

![](./week03/snap-perfect.jpg)


## Back to FreeCAD - 3D parametric modeling

### Some insides

After last week's failure, I had a video call with one of the FreeCAD forum's members (nice community there!) He told me that I should use some geometrical sketches and use a "sub-object shape binder" to create the *pad* body. Another thing that he told me, is I don't need to create a Datum plane. If there is a normal line in a sketch and a point (vertex) I want my new plan sketch to pass by, I just need to select those and "create a new sketch".

![freecad-new-normal-sketch](./week03/freecad-new-normal-sketch.jpg)

Then FreeCAD will automatically propose some propositions for the orientation/attachment of the new sketch. In my case I selected "NormalToEdge".

![freecad-sketch-attachmen](./week03/freecad-sketch-attachment.jpg)

### New parameters for the Scale Model

For the 5mm plywood scale model, I create new variables that I'll use for the design.

| **Scale Model**      |             |     |   |   |
|----------------------|-------------|-----|---|---|
| plywood_thickness    | 5           | mm  |   |   |
| bolt_diameter        | 5           | mm  |   |   |
| fourBars_thickness   | 15          | mm  |   |   |
| frameLarge_thickness | =100 * $B$1 | mm  |   |   |
| snap_thickness       | 3           | mm  |   |   |
| snap_deflection      | 0.5         | mm  |   |   |
| snap_exit_angle      | 100         | deg |   |   |
| snap_entrance_angle  | 25          | deg |   |   |
| snap_root_radius     | 1           | mm  |   |   |
| frameSmooth_radius   | =100 * $B$1 | mm  |   |   |
| tolerance            | 0.1         | mm  |   |   |


### Drawing the shapes and first bodies

After that, I started to draw the sketches that I will use to create the bodies of the scale model. Starting by the rear of the trike.

![](./week03/freecad-rearwheel-sketch.jpg)

As I wanted to use [LCInterlocking module](https://github.com/execuc/LCInterlocking) that will let me generate finger joints automatically, I draw my shape to both pads that will touch each other but not intersect.

![](./week03/freecad-touching-pads.jpg)


Then I started to extrude some "pad" from the sketches. Setting the *length* to the parametric value `plywood_thickness` (in my case 5mm). 

![](./week03/freecad-newpad.jpg)


When the 3 rear pads have been done, I wanted to try the mirror function to create the left part of the tricycle. I select the pad in the body to mirror it. 

![](./week03/freecad-mirror.jpg)

Back to the vertical sketch and let draw the main frame. But I first created the front wheel pad. And then select the face where the frame will be fixed to.

![](./week03/freecad-FlatFace-attachment.jpg)


And when the sketch is done, the same for the back of the main frame.

![](./week03/freecad-FlatFace-attachment2.jpg)


### Let's make some joints

The first joints I wanted to try are the Snap-fit joint to link the main frame to the rear four bars linkage.


![](./week03/freecad-snap-fit.jpg)

Now I can start the last sketch I'll need for this scale model. So I selected the face between the snap-fit joint.

![](./week03/freecad-rear-sketch.jpg)

Like I did with the previous sketches, I had to import some lines with the `Add external geometry` function.

![](./week03/freecad-import-external.jpg)

From that sketch, and still using the `sub-object shape binder`, I created the right side of the *four bars linkage*. I also created the pad from the previous sketch *rearWheel_part* so I have the entire right side of the trike.

![](./week03/half-complete.jpg)

Again, let's use the mirror function to create the left side. Like before, I select the body and the pad I want to mirror, go to the ``mirrored` function, and chose the _Base_ XY plane* as the symmetrical plane.

![](./week03/freecad-mirror-rear.jpg)

And here we go, the 3D scale model is ready.

![](./week03/final_mockup.jpg)

## Laser cut the scale model

As I said, there is a laser module that you can add to FreeCAD. I went to Tools > Addon Manager and looked for *LCInterlocking*. I learned how to use the LCInterlocking Workbench with [this youtube tutorial](https://www.youtube.com/watch?v=6sKECZwBjZk).

![](./week03/LCInterlocking.jpg)

So first, I created a copy of all the bodies (so I don't mess with them). For that, I use the `simple copy` function in the *Part Workbench*.

![](./week03/freecad-copys.jpg)

Then toggle the visibility of the first bodies with the `sapce bar` shortcut. Then I moved on to the _Laser Cut Interlocking Workbench_ (LCInterlocking). 

The support of the rear wheels will be joined with finger joints, so I selected the 3 bodies and click on the `Slots` function and click on `Add same parts`.

![](./week03/freecad-parts-fingerjoints.jpg)

Then you can select the face where you want some tabs for the finger joint.

![](./week03/freecad-faces-fingerjoints.jpg)

You can choose how many tabs you want on each face. I choose 1 for the smallest one to 3 for the longest.

And finally, I clicked on the first part, I added the parameters of my Laser Cutter Machine. Regarding what I found in the joint clearance section](./week03.md#joint-clearance), I chose $0,26mm$ for the laser diameter (kerf) and $0,01mm$ for the slot width tolerance. Then I hit `OK`.

![](./week03/freecad-laser-params.jpg)

A new part has been created called *MultiJoin* with all the tabs made and an offset for the kerf and clearance. I selected the parts in the *MultiJoin* and click on the `Export` button.

:::caution

Once the operation is done, the link between the original parts has been lost. So if you need to change some parameters I'll need to redo the last steps.

:::

![](./week03/joint-export.jpg)


It creates a new FreeCAD file from where I can export the 2D DXFs files.

![](./week03/freecad-shape2D-view.jpg)

For the other parts, I need to flatten the face, so I went to the *Draft Workbench* and use the `Shape 2D View` function. Being perpendicular to the face, I select the face and then project it on the XY plane with the function. 

![](./week03/freecad-2D-top.jpg)

When it's done I can select all the Shape2DView and `Export` them (Files > Export).

![](./week03/freecad-export-frame-dxf.jpg)


## Scale model assembly

In the end, I had two DXF files: [`mockup-laser-frame.dxf`](../static/files/mockup-laser-frame.dxf) and [`mockup-laser-frame-rear.dxf`](../static/files/mockup-laser-frame-rear.dxf). The first DXF has been cut with the $0,12mm$ offset from the [joint clearance](./week03.md#joint-clearance). But for the second one, the LCInterlocking workbench has already set the right offset.

:::caution

I had to be careful to set the offset to the *_inner_ or *_outer_ shape of the piece.

:::

![](./week03/model-assembly-1.jpg)
![](./week03/model-assembly-2.jpg)
![](./week03/model-assembly-3.jpg)
![](./week03/model-assembly-4.jpg)

<video controls width="80%">
  <source src={scaleModel}/>
</video>


> **Notes:**
> Disepointed by the LCInterlocking automatic offset, some joints was prefect, but not all of them.
> I made a mistake on my model. I actually draw the scale model before doing the snap-fit joint tests. I forget to change the parameters in the scale model. So one of my snap-fit joint broke. I had to glue it. (I saw it too late to recut a new piece).

## Vinyl cutting

If you look at the scale model there is a vinyl mosquito. For that, I used the plotter of the Fablab ([Graphtec CE6000](https://www.promattex.com/plotter-traceur-decoupe/graphtec/graphtec-ce6000-60.php)). I decide to transfer the logo of the [OpenSource Hardware Mosquito Project](https://mosquito-velomobiles.org/) on the scale model to put at the rear of the trike.

![mosquitoos logo](./week03/logo-mosquito-os.svg)

I imported the vector logo in DXF in [Graphtec Studio](http://www.graphteccorp.com/product/soft/gs/index.html). I chose a size that pleased me.

![size vinyl](./week03/size-vinyl.jpg)

With a force and speed set to $17$ and *30cm/s$ I send the draw to the machine

![size vinyl](./week03/send-to-cutter.jpg)

I had to redo it as I stupidly cut the logo on the test force of the machine.

![second vinyl](./week03/vinyl-second.jpg)

I tore off the vinyl with a paper transfer.

![vinyl-1](./week03/vinyl-1.jpg)
![vinyl-2](./week03/vinyl-2.jpg)
![vinyl-3](./week03/vinyl-3.jpg)
![vinyl-4](./week03/vinyl-4.jpg)
![vinyl-5](./week03/vinyl-5.jpg)
![vinyl-6](./week03/vinyl-6.jpg)
![vinyl-7](./week03/vinyl-7.jpg)

*Update 12/03/2023*

## Press-kit

As I misunderstood the assignment with my scale model, here is a small press kit. I decided to design the 2D drawing on Fusion 360. So first, let's declare a variable of the cardboard's thickness.

![](./week03/press-kit1.jpg)

I use the *mirror function* to create the cardboard slot.

![](./week03/press-kit2.jpg)

I added a chamfer, to make it easier to put the cardboard polygons together.

![](./week03/press-kit3.jpg)

With the _Circular pattern_ tool, I created all the other slots.

![](./week03/press-kit4.jpg)

I then trimmed (T) the openings of the slots and exported the `DXF` file.

![](./week03/press-kit5.jpg)

In the *LaserCut* software, I imported the `DXF` and duplicate the shape with the *Rectangular Pattern*, called *Copys* in the software.

![](./week03/press_kit_lasercut.jpg)

I then sent the job to the laser with the right parameters. And here is the results:

![](./week03/presskit-final.jpg)


## Files

Here are the CAD files for the week.

* [export-laser-shape.FCStd](../static/files/export-laser-shape.FCStd)
* [final-project-mockup.FCStd](../static/files/final-project-mockup.FCStd)
* [mockup-laser-frame.dxf](../static/files/mockup-laser-frame.dxf)
* [mockup-laser-frame-rear.dxf](../static/files/mockup-laser-frame-rear.dxf)
* [snap-fit-joints.FCStd](../static/files/snap-fit-joints.FCStd)
* [snap-fit-joints-3mm-1mm.dxf](../static/files/snap-fit-joints-3mm-1mm.dxf)
* [snap-fit-joints-2mm-1-5mm.dxf](../static/files/snap-fit-joints-2mm-1-5mm.dxf)