---
sidebar_position: 1
---


# Final project

![](../static/presentation.png)

<video controls width="100%">
  <source src="/2023/labs/kamplintfort/students/dorian-somers/presentation.mp4"/>
</video>


* All Hardware files (DXF, KiCAD files, Fusion360 Files, GCodes, SCAD, FreeCAD files, CAM files) are licensed **CERN-OHL-W**.
* All code files (Arduino INO, C++ code, Python files, Javascript files) are licensed **GPLv3**.
* All all textes, drawings, screenshots and pictures originally made for this project are under copyright **CC-BY-SA**.

The licenses are at the root of the repository. [More info on Week17 page](/weekly-assignments/week17#intellectual-property).

## Concept

import Concept from './_concept.mdx';

<Concept/>



## Inspirations and similar projects

import Inspirations from './_inspirations.mdx';

<Inspirations/>


## Project development

import Gantt from './_gantt.mdx';

<Gantt/>

## Bill of Materials

### Traffic Sign

import BOMTrafficSign from './02-Traffic-sign/BOM-traffic-sign.mdx';

<BOMTrafficSign/>



### Wally

import BOMWally from './03-Wally/BOM-wally.mdx';

<BOMWally/>

## Future developments

It will be nice to develope a bit more this project, to try to equip a road with some signs. More infos on what could be made for the future of the project [on the week17 page](/weekly-assignments/week17#opportunities-and-business-model).