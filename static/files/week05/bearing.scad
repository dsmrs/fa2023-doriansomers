$fn=50; //number of faces for the circle.

inner_bearing_radius = 4; //the inner radius where the bold will be
outer_bearing_radius = 16; //the outside radius of the bearing

bearing_thickness = 6; //depth/thickness of the bearing

bearing_ring_thickness = (outer_bearing_radius-inner_bearing_radius)/2-bearing_thickness/4; //thickness of the 2 rings (inner and outer)

tolerance = 0.2; //distance between the 2 rings and the balls

cutout_radius = bearing_thickness/2+tolerance; //radius of the circle that will "cut" the rings

translate_radius_balls = (outer_bearing_radius+inner_bearing_radius)/2;

balls_radius = bearing_thickness/2; //The balls have the same diameter as the bearing's thickness so it can be printed.

number_of_balls = floor((2*PI*translate_radius_balls)/(2*balls_radius)); // take the biggest number of balls that could go around the balls circle.

angle_between_balls = 360/number_of_balls; // Devide 360 degrees by the number of balls

echo(number_of_balls)

// The 2 rings are made a rotate extrude from the 2 rectangles subtracted
rotate_extrude(){
    // the two rectangles are substracted with a circle of a radius same as the balls + a tolerance
    difference(){
        union(){
            //inner ring is first a rectangle
            translate([inner_bearing_radius+bearing_ring_thickness/2,0,0]) square([bearing_ring_thickness,bearing_thickness], center = true);
            // Same for the second one
            translate([outer_bearing_radius-bearing_ring_thickness/2,0,0]) square([bearing_ring_thickness,bearing_thickness], center = true);
        }
        translate([translate_radius_balls,0,0]) circle( cutout_radius ,$fn=50) ;
    }
}

// The balls are sphere move to a certain angle
for ( ball = [0 : angle_between_balls : 360] ) {
    rotate([0,0,ball])
    translate([translate_radius_balls,0,0])
    sphere( r=balls_radius );
}
