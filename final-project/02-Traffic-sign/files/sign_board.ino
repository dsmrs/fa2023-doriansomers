#include <SPI.h>
#include <LoRa.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>

#define SS 0          // LoRa CS PPIN
#define PIN_BIKE 1    // Bike PWM PIN
#define PIN_MATRIX 2  // Matrix LED PWM PIN


Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(
    32,                  // Width of the Matrix in LED
    8,                   // Hieght of the Matrix  in LED
    PIN_MATRIX,          // Data pin
    NEO_MATRIX_TOP +     // Top/Bottom location of the first LED
    NEO_MATRIX_LEFT +   // Left/Right location of the first LED
    NEO_MATRIX_COLUMNS + // Arrangement vertical of LEDs
    NEO_MATRIX_ZIGZAG,   // Layout of the LEDs
    NEO_GRB +            // Pixels are wired for GRB bitstream
    NEO_KHZ800           // bitstream for WS2812 LEDs
);

const uint16_t TextColor = matrix.Color(255, 0, 0);

int bikeState = LOW;  // bikeState used to set the LED
String warning[] = {"BIKE", "ON", "THE", "ROAD"};
int cursors[] = {4, 11, 8, 4};
int arrayLength = 4;
int indexState = 0;

unsigned long previousMillis = 0;  // will store last time LED was updated

const long interval = 1000;  // interval at which to blink/switch words (milliseconds)

bool signOn = false;    // Whenever the sign in ON of OFF.
bool msgReceived = false;

int intervalsCount = 0; // count the intervals
const int maxInterval = 60; // set how many intervals the sign will be on.

void setup() {
  Serial.begin(9600);
  delay(5000);
  Serial.println("Bike Sign Board");
  matrix.begin();
  matrix.setTextWrap(false);
  matrix.setBrightness(100);
  matrix.setTextColor(TextColor);
  pinMode(PIN_BIKE, OUTPUT);
  LoRa.setPins(SS);
  if (!LoRa.begin(868E6)) { // Start LoRa with a frenquency of 868Mhz
    Serial.println("Starting LoRa failed!");
  }
}

void loop() {
  int packetSize = LoRa.parsePacket();
  if (packetSize && !msgReceived) {
    // received a packet
    Serial.print("Received packet '");

    // Read the received data
    String receivedData = "";
    while (LoRa.available()) {
      char c = LoRa.read(); // Read a character from the packet
      receivedData += c; // Append the character to the string
    }
    if (receivedData == "bike") {
      signOn = true;
      msgReceived = true;
    }
    if (receivedData == "merci") {
      printOnScreen("Merci",2);
    }
  }
  
  if (signOn) { // check is the Sign should be on
  //if (true) { // check is the Sign should be on

    unsigned long currentMillis = millis();

    if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis; // save the last time LED blinked
      printOnScreen(warning[indexState], cursors[indexState]); // print a word on the screen
      if (++indexState >= arrayLength) indexState = 0; //incremente the index
      if(++intervalsCount >= maxInterval) { // the max Interval is reached the Sign is turn off and all the variable reset the their original values.
        signOn = false;
        msgReceived = false;
        previousMillis = 0;
        bikeState = LOW;
        indexState = 0;
        intervalsCount = 0;
        printOnScreen("",0);
        digitalWrite(PIN_BIKE, bikeState); // set the LED with the correct state
      } else {
        bikeState = !bikeState; // inverse the LED state for the next run
        digitalWrite(PIN_BIKE, bikeState); // set the LED with the correct state
      }
    }
  }
}

void printOnScreen(String msg, int cursor) {
  matrix.fillScreen(0);
  matrix.setCursor(cursor, 0);
  matrix.print(msg);
  matrix.show();
}
