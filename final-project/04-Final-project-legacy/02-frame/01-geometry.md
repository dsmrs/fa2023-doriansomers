---
draft: false
sidebar_position: 1
---

# Geometry of the tricycle

:::caution

It's a WIP documentation page.

:::

![](./img/nomanclature.jpg)

![4bars linkage](img/4barslinkage.gif)

A better way to understand the direction system of the trike. (source: www.mosquito-velomobile.com)

There are some vocabulary about [bike geometrie we need to know first](https://en.wikipedia.org/wiki/Bicycle_and_motorcycle_geometry).


![geomtry](img/geometry.jpg)