
# Frame

## Concept

<div className="steps">

![](./traffic-sign.jpg)

Here are the basic components in the Traffic sign, to alert that a bike is coming.

</div>

## CAD

import trafficSignCAD from './traffic-sign-cad.mp4';

<video controls width="80%">
  <source src={trafficSignCAD}/>
</video>

A first proposition for the Traffic Sign bike. Here the physical components of the sign:

1. Top layer (1.in aluminium with a reflective top (1.5mm)
2. A full glass acrylic layer of 2mm
3. En LED enclosure.layer (2) in plywood (5mm)
4. A Matrix LED. 8x32 (WS2812b)
5. A bike in Red LEDs
6. An aliminium layer (3),support for the LEDs (heat dissipation)
7. Behind this layer, a board with MCU to command the LED and a LoRa module to recieive the informations
7. Board enclosure layer (4) in plywood (5mm)
8. A last finishing back layer (5) in plywood (5mm)


<div className="steps">

![](./frame/bike_drawing.jpg)

So first I download a picture of a French Bike Traffic Sign and redraw a parametric bike on top of it.

![](./frame/ws2812b-8x32-led-matrix.jpg)<br/>*source [zedfy.shop](https://www.zedfy.shop/8x32-ws2812b-led-matrix)*

Knowing the dimension of the Matrix LED I wanted to use, I needed to create some hole to let the light pass throught the aluminium layer.

![](./frame/matrix-led-cad.jpg)

Each hole has a diameter of 5mm as it's the size of the [ws2812b led](https://duckduckgo.com/?q=ws2812b+size).

![](./frame/layer2.jpg)

After the first aluminum layer and the acryclic, I have a thick layer that will let me put the lights components and also put the LoRa antenna, and I started to thing about the hole to pass different bolts to fix the layer together and also the Sign to a pole.

![](./frame/layer3-cad.jpg)

Then there is another aluminium layer. On the front, it will hold the matrix LEDs and the bike LEDs, than there are some holes to let the wires to pass throught the rear of the layer that will hold the MCU and the LorA module.

![](./frame/layer4-cad.jpg)

As the first layer of plywood, there is a second one to have some room to put all the components and let an entrance of the power supply wires.

![](./frame/layers_holes_cad.jpg)

Fome of the informations needed have been made after the drawing of the layers. Like for example the hole between the layers to fix them. 

![](./frame/round_corner_cad.jpg)

Or when a added a round fillet to the corners when I decied to use the plasma CNC cutter instead of the CNC milling machine.

![](./frame/projection_layers_cad.jpg)

The layers will be cut with 2D machines: plasma and laser cutting machines. They need clean DXF files to generate the toolpath. So I recreated some sketch that are just a projection of the different faces of the Sign. So the DXF is cleaner than the orignal one with all the constrains, but stil connected to the it. So if I change anything the sketch will changed too.

![](./frame/vinyl_pcb.jpg)

Even if I used Fusion 360 to create the Bike LEDs PCB, I decided to documented it on the [*Electronic section*](/final-project/Traffic-sign/electronics#bike-led), so it can be found there.

</div>

## Plasma cut aluminium.

The first layer and the 3rd one, will be in aluminum. They are the more complex to make so let's start with them.

<div className="steps">

![](./frame/plasma5.jpg)

I wasn't sur first to go with the plasma cutter, after the result the the [Wuld Card week](/weekly-assignments/week15#on-the-machine). So I did a first try with the CNC, but the aluminium sheet became so hot that it began to warp.

![](./frame/Fastcam1.jpg)


So back to [FastCAM](https://fastcam.com/). I tooked on the the first layer. I choose a kerf or $1,6mm$ as on the Wild Card week, the value of $1,5mm$ wasn't enough.


![](./frame/error-code.jpg)

I try to cut the first layer, but it stopped after the bike cut with an `0-30` error on the plasma station. After having a look at the Hyperthem documentation, it happen that the electrode was too worn out.


![](./frame/plasma6.jpg)

We have some consumables, so I changed it.



![](./frame/plasma7.jpg)

It looks great! The plasma is soo quick to cut this alumnimum sheet. I then cut the seconde aluminum layer.

![](./frame/plasma8.jpg)

When I put the two layers on top of each other, it's  a match!

![](./frame/plasma9.jpg)

... but ... When I reverse one of them, the bottom corners do no longer match. 

![](./frame/mistake_roundcorner.jpg)

It loooks like I didn't constrain well my sketches. When moving something (I don't know what) the corners have changed. As I don't have enough time, I'll let it like this. I may go for a v2 with a more clean aluminum sheet cut.


</div>

## CNC milling the aluminium layer

The lateral cutting was a failure, but for vertical drilling of small holes, the CNC remains the ideal tool. 

<div className="steps">

![](./frame/cam_cnc1.jpg)

As my layers are already cutted, I need to proply set the origin. So in the CAM of Fusion 360, I design a **inner** contour to cut into some wood, to be able to set the aluminium triangle into it.

![](./frame/aluminium2.jpg)

I just use some thin leftovers. I just need the corners to be into the wood not all of it.


![](./frame/plasma10.jpg)

I then use another piece of wook, a bit stronger, to fix the triangle (using the holes into the frame of the screws).

![](./frame/cam_cnc2.jpg)

Because of the piece of wood, I had to change the *retract height* of the tool to be sure it won't hit the wood during his job.

![](./frame/plasma13.jpg)

The milling went well. Even if the sheet warpped a bit. I didn't wanted to burn the scrificial layer on the bottom, I set the final stepdown to the thickness of the aluminium sheet. As the alumnium welt a bit, some holes were not pierced. So I had to remove what was left manually with a cutter.

</div>

## Laser Cut the wood players

<div className="steps">

![](./frame/laser_layer.jpg)

There are 4 layers that are cut with the laser. 3 are in plywood (5mm) and 1 un acrylic glass. To have a clean `DXF` file I decided to create new sketches in fusion and I used the project function du get the layer's face. And then export the sketch in `DXF`.

![](./frame/acrylic4.jpg) 

I knew the parameters for cutting 5mm plywood (cfr. [week3](/weekly-assignments/week03#power-and-speed)). But I had to do some tests for the glass acrylic.

![](./frame/acrylic3.jpg)

So here I can see that $30mm/s$ and $30 \%$ (speed and power) are the parameters to cut, and the $50mm/s$ and $10 \%$ is already good to mark (I'll mark the bicycle draw on it).

![](./frame/layers.jpg)

So here are all the layers cut. (Plasma cutting pieces include)

![](./frame/acrylic2.jpg)

I want a more *_diffuse_ light behind the "bike LED". So the idea is to sand a bit the acrylic to help with that. Here is a first test we can see the difference.

![](./frame/acrylic1.jpg)

And here we go. I have now all the parts of the frame. Let's go for the electronics.

</div>


## Files

Here is the view of the Fusion 360 file for the frame. You can [download here](./files/FA2023_final_project_traffic_sign.f3d).

<iframe src="https://myhub.autodesk360.com/ue2d55e35/shares/public/SHd38bfQT1fb47330c99ce97d047fbbc3643?mode=embed" width="100%" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

Here are the other files I used to make the frame:

* [L3_cont.dxf](./files/L3_cont.dxf) 
* [L3_cont.nc](./files/L3_cont.nc) 
* [L3_drill.nc](./files/L3_drill.nc) 
* [L4_cont.dxf](./files/L4_cont.dxf) 
* [L5_cont.dxf](./files/L5_cont.dxf) 
* [layer1_bike_alu.dxf](./files/layer1_bike_alu.dxf) 
* [layer3_board_alu.dxf](./files/layer3_board_alu.dxf) 
* [PLASMA_L1.NC](./files/PLASMA_L1.NC) 
* [PLASMA_L1_bonus.NC](./files/PLASMA_L1_bonus.NC) 
* [PLASMA_L3.NC](./files/PLASMA_L3.NC) 
* [cont_supp.nc](./files/cont_supp.nc) 
* [L1_cont.dxf](./files/L1_cont.dxf) 
* [L1_cont.nc](./files/L1_cont.nc) 
* [L1_drill.nc](./files/L1_drill.nc) 
* [L2_cont.dxf](./files/L2_cont.dxf)