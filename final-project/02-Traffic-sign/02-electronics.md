
# Electronics

## Graph

Before doing the Schematic in FreeCAD, I like to draw the graph with the link of all the components.
 
import Graph from './_graph.mdx';

<Graph/>

## Board

<div className="steps">

![](./electronics/kicad1.jpg)

The board of the Sign is not too complex. I made a quick design of the board with the MCU (an XIAO SAMD21) and the pinout I had to expose. So there are the 4 pinouts for the SPI protocol + 3.3 and GND for the LoRa module. The is a digital pin to control a Mosfet to turn on and off the Bike LED and another one to command the Matrix LED.<br/> I added also some pads to connect the power supply and to give more power to the Matrix LED.

![](./electronics/kicad2.jpg)

I wanted a single-layer PCB, so I made the tracks so I don't need vias. The LEDs (for the Bike and for the Matrix) can consume a lot of power. For security, I made really thick tracks for the input 5v and the outputs that go to the LED. With [this calculator](https://www.omnicalculator.com/physics/wire-size), those tracks should be able to handle more than $800mA$. For my use of the Matrix LED, it will be enough.

![](./electronics/board_sign_pic.jpg)

And here is the result. I solder 1 or two pinheads more so it's better attached to the board (Surface Mounted pinheads are weaker than Through Holes)

</div>

## Bike LED

<div className="steps">

![](./electronics/common-res.jpg)<br/>*source [circuitbread.com](https://www.circuitbread.com/tutorials/why-cant-i-share-a-resistor-on-the-common-anode-or-cathode-of-my-rgb-led)*

After some readings, I discovered, that **I can not share one resistor for an array of LED**, so I'll have to have to pair resistors with each LEDs. One resistor for multiple LEDs in serie is OK (and the order of it doesn't matter), but not in parallel.

![](./electronics/bike_schema.jpg)

So here is the schematic idea. To find the right values of the resistors, I used this formula:<br/>$R = (5V-For_{Volt})/0,02A$[^1]<br/> Where $For_{Volt}$ is the forward voltage of my LED (~2V for my red LED) so I need a resistance of $50 \Omega$ when there are two LEDs and a branch and $150 \Omega$ when the LED is alone.

[^1]: https://www.homemade-circuits.com/how-to-calculate-and-connect-leds-in/

![](./electronics/bikeLED.jpg)

But how could the LEDs be placed in a bike shape. I draw an idea in an old paper (with coffee on it). But I quickly realized that "led curtain" would be much easier to make (on the top right of the drawing).

![Alt text](./electronics/led-footprint.jpg)*source [lighthouseleds.com](https://i0.wp.com/lighthouseleds.com/media/catalog/product/1/2/1206-smd-led-drawing-schematic-dimensions-footprint_8.jpg)* 

As it's a complex vectoral draw, I won't use KiCAD, but Fusion 360 instead. I just needed the footprint of my SMD LED and also the SMD 1206 electric resistances.

![](./electronics/vinyl_pcb.jpg)

So with the bike in the background, I draw this. With my learnings from [week15](/weekly-assignments/week15#pcb-with-a-vinyl-plotter), I tried to draw the lines as straight as possible. It took me more than 4 hours to draw... an infinity! I should find a better way next time.

![](./electronics/PCB_copper1.jpg)

Using the parameters discovered [in week15](/weekly-assignments/week15#pcb-with-a-vinyl-plotter), I quickly launch the cut.

![](./electronics/PCB_copper2.jpg)

Here we go when peal off and put it on a reflective sheet of plastic (actually use for t-shirt transfer).

![](./electronics/PCB_copper3.jpg)

Damm... When I imported the `DXF` on the vinyl cut programm, the software made a scale mistake, a small one, but now it doesn't fit in the frame. So I'll need to make another one.

![](./electronics/PCB_copper4.jpg)

I then solder the LED with their resistances. I used solder paste, as the melting temperature is low to not burn the copper vinyl.

![](./electronics/PCB_copper5.jpg)

Then test the continuity along the way.

![](./electronics/PCB_copper6.jpg)

Tadaa, it's Christmas! It actually do not drive as much current. Less than 200mA.

</div>


## Code for the MCU

To be able to start to code before having all the parts, I looked for some [Arduino Simulator](https://all3dp.com/2/best-arduino-simulators-online-offline/). I didn't found one that was supporting my LoRa module, but [Wokwi](https://wokwi.com/) is able to simulate Matrix LED, pushbutton et OLED display, so I'll start with it.

### Control the LED with an N-Mosfet.

I have some IRF520N N-Mosfet. They can manage up to $9,5 A$. As my LEDs need around 20mA, it means to I can put $\frac{9,5A}{0,02A}=475$ LEDs on the bike LEDs, it's more than enough.

**schematics of the bike led**

All we need is to connect the 5V to the LEDs and the "Ground" of the LED to the *Drain* of the Mosfet. The *Gate* will be connect to a pin of the board, and then the *Source* go to the actual Ground.

Here is a simple code to blink the LED.

```cpp
#define PIN_BIKE 3 // Ouput of the Gate Mofet pin

void setup() {
  pinMode(PIN_BIKE, OUTPUT);
}

void loop() {
  digitalWrite(PIN_BIKE, HIGH);
  delay(500);
  digitalWrite(PIN_BIKE, LOW);
  delay(500);
}
```

### Control de Matrix LED.

<div className="steps">

![](./electronics/matrix_led_single_line.jpg)

As explained in the [frame section](/final-project/Traffic-sign/frame#cad), the Traffic sign has a 32 by 8 Matrix LEDs. Those 256 WS2812b RGB addressable LEDs are linked together in a *Zigzag* layout. But in the end, it's similar to an LED strip. 

</div>

For that, Adafruit has created a nice library called [NeoMatrix](https://learn.adafruit.com/adafruit-neopixel-uberguide/neomatrix-library). This library needs the _GFX_ and the *NeoPixel* library to work. One included, I first create an instance regarding my configuration.


```cpp
#include <Adafruit_GFX.h>
#include <Adafruit_NeoPixel.h>
#include <Adafruit_NeoMatrix.h>

#define PIN 6 // Where the Matrix is connected on the board

Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(
    32,                  // Width of the Matrix in LED
    8,                   // Hieght of the Matrix  in LED
    PIN,                 // Data pin
    NEO_MATRIX_TOP +     // Top/Bottom location of the first LED
    NEO_MATRIX_RIGHT +   // Left/Right location of the first LED
    NEO_MATRIX_COLUMNS + // Arrangement vertical of LEDs
    NEO_MATRIX_ZIGZAG,   // Layout of the LEDs
    NEO_GRB +            // Pixels are wired for GRB bitstream
    NEO_KHZ800           // bitstream for WS2812 LEDs
);
```

Then I can set some parameters of the matrix object, like the color of the text or the brightness of the LEDs.

```cpp
const uint16_t TextColor = matrix.Color(255, 0, 0); // Choose Red Color for

void setup() {
  matrix.begin();
  matrix.setTextWrap(false);
  matrix.setBrightness(100);
  matrix.setTextColor(TextColor);
}
```




```cpp
void loop() {
  printOnScreen("Hello");
  delay(500);
  printOnScreen("World");
  delay(500);
}

void printOnScreen(String msg) {
  matrix.fillScreen(0);
  matrix.setCursor(0, 0);
  matrix.print(msg);
  matrix.show();
}
```

import matrixLED from './electronics/text-matrix-led.mp4';

<video controls width="80%">
  <source src={matrixLED}/>
</video>


### No more Delay

Delays are cool, but they pause the program. So we can't listen to anything else (pushbutton, lora signal, etc.).If I choose the RP2040 board, as it has 2 cores, I could use one thread for the LED and the other one to listen to any Lora Signal.

But I preferred to look for a more versatile code using [this tutorial](https://docs.arduino.cc/built-in-examples/digital/BlinkWithoutDelay).

So first I added some new variables before the setup.

```cpp
int bikeState = LOW;  // bikeState used to set the LED
String warning[] = {"Bike", "on", "the", "Road"};
int arrayLength = 4;  // As there is not simple *length* function in cpp
int indexState = 0;

unsigned long previousMillis = 0;  // will store last time LED was updated


const long interval = 1000;  // interval at which to blink/switch words (milliseconds)
```

And here is the new loop:

```cpp
void loop() {
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis; // save the last time LED blinked
    printOnScreen(warning[indexState]); // print a word on the screen
    if (++indexState >= arrayLength) indexState = 0; //incremente the index
    digitalWrite(PIN_BIKE, bikeState); // set the LED with the correct state
    bikeState = !bikeState; // inverse the LED state for the next run
  }
}
```

Here is an example that say *Bike in 100m*.

import noDelay from './electronics/no-delay-leds.mp4';

<video controls width="80%">
  <source src={noDelay}/>
</video>


### Listen to LoRa

Now, that I don't pause my program anymore, I can listen to any LoRa message. For that I'll that the *Receiver* part in my [Network and communication week](/weekly-assignments/week13#play-with-lora).

So first I need more library and set LoRa's configurations.

```cpp
...
#include <SPI.h>
#include <LoRa.h>

...

#define SS 0 // where is the LoRa module connected

...

bool signOn = false;

int intervalsCount = O; // count the intervals
const int maxInterval = 60; // set how many intervals the sign will be on.

void setup() {
  ... // previous setup

  LoRa.setPins(SS);
  LoRa.begin(868E6) // Start LoRa with a frenquency of 868Mhz
}
```

Then I can watch for any Packet received by LoRa.

```cpp
void loop() {
  // try to parse packet
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    // received a packet
    Serial.print("Received packet '");

    // Read the received data
    String receivedData = "";
    while (LoRa.available()) {
      char c = LoRa.read(); // Read a character from the packet
      receivedData += c; // Append the character to the string
    }
    If (receivedData == "bike") {
      signOn = true;
    }
  }
  ...
}
```

Once the string `"bike"` received I putted the `signOn = true`, so I can reuse the LEDs code to light on the traffic sign for 60 interval (= $60secondes$).

```cpp
void loop() {
  ...
  if (signOn) { // check is the Sign should be on

    unsigned long currentMillis = millis();

    if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis; // save the last time LED blinked
      printOnScreen(warning[indexState]); // print a word on the screen
      if (++indexState >= arrayLength) indexState = 0; //incremente the index
      if(++intervalsCount >= maxInterval) { // the max Interval is reached the Sign is turn off and all the variable reset the their original values.
        signOn = false;
        previousMillis = 0;
        bikeState = HIGH;
        indexState = 0;
        digitalWrite(PIN_BIKE, bikeState); // set the LED with the correct state
      } else {
        digitalWrite(PIN_BIKE, bikeState); // set the LED with the correct state
        bikeState = !bikeState; // inverse the LED state for the next run
      }
    }
  }
}
```

## Let's put this together

<div className="steps">


![](./electronics/assembly1.jpg)

The intermediare layer in alimnium get all the electronics. There are holes for wires to pass through.

![](./electronics/assembly2.jpg)

The penultimate layer in wood, let me arrange a bit the cables.

![](./electronics/led_on.jpg)

So let's do a simple LED light on test. It's working!

import stopMotion from './electronics/stopmotion_sign.mp4';

<video controls width="100%">
  <source src={stopMotion}/>
</video>

So let's finish the assembly and I can work on [the Wally LoRa module.](/final-project/Wally/case)

</div>

## Files

Here are the files I used to make the Bike LED PCB and the Board and the code.

* [PCB_vinyl.f3d](./files/PCB_vinyl.f3d)
* [pcb_bike.dxf](./files/pcb_bike.dxf) 
* [sign_board.ino](./files/sign_board.ino)
* [traffic_sign_board.kicad_pro](./files/traffic_sign_board/traffic_sign_board.kicad_pro) 
* [traffic_sign_board.kicad_pcb](./files/traffic_sign_board/traffic_sign_board.kicad_pcb) 
* [traffic_sign_board.kicad_sch](./files/traffic_sign_board/traffic_sign_board.kicad_sch) 
* [traffic_sign_board-PTH.drl](./files/traffic_sign_board/traffic_sign_board-PTH.drl) 
* [traffic_sign_board.kicad_prl](./files/traffic_sign_board/traffic_sign_board.kicad_prl) 
* [traffic_sign_board-B_Cu.gbr](./files/traffic_sign_board/traffic_sign_board-B_Cu.gbr) 
* [traffic_sign_board-Edge_Cuts.gbr](./files/traffic_sign_board/traffic_sign_board-Edge_Cuts.gbr) 
* [traffic_sign_board-F_Cu.gbr](./files/traffic_sign_board/traffic_sign_board-F_Cu.gbr) 
* [traffic_sign_board-job.gbrjob](./files/traffic_sign_board/traffic_sign_board-job.gbrjob) 
* [traffic_sign_board-NPTH.drl](./files/traffic_sign_board/traffic_sign_board-NPTH.drl)