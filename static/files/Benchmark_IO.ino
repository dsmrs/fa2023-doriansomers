/* Benchmark V2.0 02-June-2015  */

#define targetBoard "Arduino UNO"
#define ideVersion "Arduino IDE v1.0.6"
#define analoguePin A0
#define digitalPin 9
#define ledPin 1

#define loopCount 50000   // Max 65535

void setup() {
  Serial.begin(115200);
  pinMode(ledPin,OUTPUT);
  digitalWrite(ledPin,LOW);
  
  pinMode(analoguePin,INPUT);
}

void loop() {
  unsigned long startTime;
  unsigned long totalTime;
  unsigned int x;
  int ar;
  
  Serial.print(F(targetBoard));
  Serial.print(F(" I/O Speed Tests Over "));
  Serial.print(loopCount, DEC);
  Serial.print(F(" Iterations. Compiled Using "));
  Serial.println(F(ideVersion));
  Serial.flush();
  delay(1000);
  
  // Digital Write
  pinMode(digitalPin,OUTPUT);
  digitalWrite(ledPin,HIGH);
  startTime = micros();
  for (x = 0; x < loopCount; x++){
    digitalWrite(digitalPin,HIGH);
    digitalWrite(digitalPin,LOW);
  }
  totalTime = micros() - startTime;
  digitalWrite(ledPin,LOW);
  Serial.print(F("Digital Pin Write Takes About "));
  //Serial.print(totalTime / 2);
  //Serial.print(F(" Microseconds And Each 'digitalWrite' Took About "));
  Serial.print((float)totalTime / (float)loopCount / 2.0, 4);
  Serial.println(F(" Microseconds."));
  Serial.flush();
  delay(1000);
  
  // Digital Read
  pinMode(digitalPin,INPUT);
  digitalWrite(ledPin,HIGH);
  startTime = micros();
  for (x = 0; x < loopCount; x++){
    ar = digitalRead(digitalPin);
  }
  totalTime = micros() - startTime;
  digitalWrite(ledPin,LOW);
  Serial.print(F("Digital Pin Read  Takes About "));
  Serial.print((float)totalTime / (float)loopCount, 4);
  Serial.println(F(" Microseconds."));
  Serial.flush();
  delay(1000);
  
  // Analogue
  digitalWrite(ledPin,HIGH);
  startTime = micros();
  for (x = 0; x < loopCount; x++){
      ar = analogRead(analoguePin);
  }
  totalTime = micros() - startTime;
  digitalWrite(ledPin,LOW);
  Serial.print(F("Analogue Pin Read Takes About "));
  //Serial.print(totalTime / 2);
  //Serial.print(F(" Microseconds And Each 'digitalWrite' Took About "));
  Serial.print((float)totalTime / (float)loopCount, 4);
  Serial.println(F(" Microseconds."));
  Serial.println();
  Serial.flush();
  delay(1000);
}


