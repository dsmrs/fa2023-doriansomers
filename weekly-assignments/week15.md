---
title: Week 15 - Wildcard Week
slug: week15
sidebar_position: 15
---

# Wildcard Week

**Individual assignment**

* Design and produce something with a digital fabrication process (incorporating computer-aided design and manufacturing) not covered in another assignment, documenting the requirements that your assignment meets, and including everything necessary to reproduce it. Possibilities include but are not limited to wildcard week examples.

## Plasma cut a Traffic Sign

<div className="steps">

![](./week15/fp-plates.jpg)

For my Final project, I need 2 aluminum plates cut with a special shape. The front and the middle layer of the [Traffic Sign](/final-project/Traffic-sign/frame)).


![](./week15/plasma-NC.jpg)

At our lab, we have a Numerical Plasma cutter. (In the picture, the machine before we move to our new place). The machine should be replugged this Monday. So I'll take the chance to cut something from my final project 

![](./week15/dual-flow-plasma-shielded.jpg)<br/>*source [Hypertherm](https://www.hypertherm.com/solutions/technology/plasma-technology/types-of-plasma/?region=EMEA)*

The plasma torch is Dual flow plasma shielded, which means the nozzle is insulated from molten metal allowing drag cuttings.[^1]

</div>

[^1]:https://www.hypertherm.com/solutions/technology/plasma-technology/types-of-plasma/?region=EMEA

### Safety

As a plasma cutter machine, there are 5 main issues regarding safety. They are documented for the Safety manual of [Hypertherm](https://www.hypertherm.com).

<div className="steps">

![](./week15/security1.jpg)<br/>*source [Hypertherm](https://www.hypertherm.com/search/?search=safety)*

**Arc rays can burn eyes and skin**<br/>So we need to cover our skin and wear a helmet to be protected from it.


![](./week15/security2.jpg)<br/>*source [Hypertherm](https://www.hypertherm.com/search/?search=safety)*

**A plasma arc can cause injury**<br/>A plasma can easily cut through gloves and skin, so keep away from the torch, do not hold metal near the cutting path.


![](./week15/security3.jpg)<br/>*source [Hypertherm](https://www.hypertherm.com/search/?search=safety)*

**Toxic fumes can cause injury or death**<br/>The plasma torch does not produce toxic fumes, but the melted metal does. Especially the stainless steel that does have a chromium layer. The fume of it contains Chromium (IV) which can be very toxic (cancer, respiratory issues, etc.). So for that we have [this fume extractor](https://www.lincolnelectric.com/en-GB/Products/le-emear-mobifilter1600m?sku=W000377938). We can also try as much as possible to remove all coating and solvents from the metal before cutting. Use good ventilation.


![](./week15/security4.jpg)<br/>*source [Hypertherm](https://www.hypertherm.com/search/?search=safety)*

**Cutting can cause fire or explosion**<br/>A torch a can be super hot and cause a fire. So we need to make sure the cutting area is safe before doing any cutting by removing all flammables within 10 meters of the cutting area


![](./week15/security5.jpg)<br/>*source [Hypertherm](https://www.hypertherm.com/search/?search=safety)*

**Machine motion can cause injury**<br/>Of course, it's still a CNC machine, so when operating it we need to be careful with the materials, the movements of the machine and the noise.

</div>

### G-code for plasma

<div className="steps">

![](./week15/kerf-comp.jpg)<br/>*source [Hypertherm](https://www.hypertherm.com/)*

Something I need to have in mind is the *kerf* of the plasma. It can change depending on the material and the Amps of the plasma cutter. Hopefully, the constructor made a good cutting chart.

![](./week15/fastcam1.jpg)

So I decided to give a first try to the simple square in square shape. At the lab, we use *FastCAM* as the plasma CAM software. In the *Path settings*, as [explained here](https://response.fastcam.com/knowledge-base/article/kerf-compensation-from-fastcam-or-fastnest_1) I can set the Kerf radius (the previous image show the diameter!).

![](./week15/fastcam3.jpg)

Then I can generate the G-Code already. When clicking on *Output NC code*. It asks for the speed for entry and exit, to set the starting point and the endpoint (I set it up to *return to the entry point*). We can already see the entry point. The plasma is starting outside of the path to not let a mark. And it is smart enough to know the inside and outside of the piece.

![](./week15/fastcam2.jpg)

Then I can check the toolpath. We can see that the kerf has been noticed. And we can see that it will start with the small square and then the outside cut.

</div>

### On the machine

<div className="steps">

![](./week15/controller12.jpg) 

Once the machine is turned on, the USB disk with the GCode on it plugs in. I can go to the *EDIT* menu (F3).

![](./week15/controller22.jpg) 

There you can then go on the *USB* (F6).

![](./week15/controller32.jpg) 

And then *LOAD* (F1). There I was able to search the USB's folder and find your G-Code.

![](./week15/controller43.jpg) 

When you have found it, you can click on *VIEW* (F7) to have a look at the toolpath. If it's good you can go back to the main menu by clicking *ESC*.

![](./week15/controller73.jpg) 

Now we can go to *AUTO* (F1).

![](./week15/controller52.jpg)

There, you can click on *VIEW* (F4) to have a look a the cut order (*N-PIECE*, F1) and the Startup point (*STARTP*, F2).

![](./week15/controller6.jpg) 

And the *AUTO* menu I can then run a test in the air by activating the _TEST_ mode (1). But before that, I had to move the head where I wanted to start with the arrows (2). Then I clicked on *START* (3) and if needed I could stop with (4).

</div>

import testAir from './week15/plasma-test-air.mp4';

<video controls width="80%">
  <source src={testAir}/>
</video>

Here is a video of a test in the air.

<div className="steps">

![](./week15/voltage-speed.jpg)

On the machine controller, I need to set the voltage and feedrate of the machine regarding my material and the Amps chosen.

![](./week15/controller7.jpg)

Those parameters can be then set on the bottom panel of the machine controller.

![](./week15/controller8.jpg)

When set, the last thing to do is to launch the path by deactivating the _test mode_ and pushing on start. For each cut of the toolpath, I had to manually restart after a small arc strike. Too quick and the arc has not been initiated, too slow and the arc has made a hole.



</div>

import cutPlasma from './week15/cut-plasma.mp4';

<video controls width="80%">
  <source src={cutPlasma}/>
</video>

Here is the video of the cut.

<div className="steps">

![](./week15/plasma-test.jpg)

The aluminum plate was really thin. So when the torch cut the corners, the speed drops down and it melts more. So the corners are not really clean. the solution will be to create some small *fillets* at the corner so the torch can keep its speed.

</div>

More Plasma cuts can be found [in the final project](/final-project/Traffic-sign/frame#plasma-cut-aluminium).

## PCB with a Vinyl Plotter

I designed a [Step Response Module for the *Input Devices week*](http://localhost:3000/2023/labs/kamplintfort/students/dorian-somers/weekly-assignments/week11#step-responses-board-with-attiny412). As I wasn't able to try to create a PCB from a copper sheet cut with a Cutting Plotter I wanted to take a moment this week to try it.

<div className="steps">


![](./week15/kiCAD1.jpg)

I try to design pretty thick tracks, but as the ATtiny412 has such a small footprint, we will see. See week 11 for more details.

![](./week15/kiCAD2.jpg)

As the Graphec Studio that pilots our plotter needs DXF files, I export with that in mind. (Files > Plot). Be careful to choose the right unit. All my components are surface mounted (SMD), so I just need the front copper layer (`F.Cu`).

![](./week15/Fusion1.jpg)

The plotter doesn't like sharp corners, so I edited the DXF file on Fusion 360 to add some radius corners.


![](./week15/coppervinyl1.jpg)

So time for some tests. The last one seems ok. It's a slow speed, $1cm/s$, the slowest possible on the machine. And the force is 14. But the corners do not like super right.

![](./week15/graphtec1.jpg)

Weirdly, when importing the modified `DXF` in Graphtec Studio, the paths looked terrible.

![](./week15/graphtec2.jpg)

So the trick I used was to export `ESP` in  with Inkscape as it's also a format that can be used in Graphtec Studio.

![](./week15/coppervinyl2.jpg)

So let's try a first test! And ... it failed ... 

![](./week15/coppervinyl3.jpg)

So I try by changing the offset of the blade. But still, the tracks are too narrow. 

![](./week15/drawing.jpg)

Based on an advice from Marius during the Open Global Time, I decided to simplify the tracks (Thanks!). So I'll make them as thick as possible and as straight as possible.

![](./week15/Fusion2.jpg)

As the circuit is pretty simple, I designed it directly in Fusion 360. The only thin tracks are the `TX` and `RX` pins, I had no choice as there are in the middle of the MCU, but there are straight. As I don't use the `PA3` pin I even could make the track of the UPDI pin a bit thicker.

![](./week15/coppervinyl4.jpg)

I decided to put a new blade on the potter. After one first fail, I used a softer force of 5 and it went well.

![](./week15/coppervinyl5.jpg)

I had some difficulty peeling off the copper, as the small tracks were coming with the exterior. But I manage to restick them.

![](./week15/coppervinyl6.jpg)

I then use a transfer paper. I was afraid of this operation as I read that is one of the most difficult parts of the job. But actually, it was straightforward.


![](./week15/coppervinyl7.jpg)

I bought also some low-temperature solder paste (138°C). I use a small soldering iron set to 300°C so it can melt the paste quickly.

![](./week15/coppervinyl8.jpg)

I was really quick and clean. I added some wires to connect `UDPI`, `TX`, `I2C` and the power.


![](./week15/coppervinyl9.jpg)

I checked if all the connections were good, and they were!




</div>

### Program the ATtiny412 Board

<div className="steps">

![](./week15/jtag2updi-setup.jpg)<br/>*source [@amidarius](https://twitter.com/amidarius/status/1184502939987185666)*

As I don't have a proper programmer, I watched [this video](https://www.youtube.com/watch?v=AL9vK_xMt4E) to flash the ATtiny412 with an Ardunio Nano. So first I uploaded [jtag2updi.ino](https://github.com/Dlloydev/jtag2updi/blob/master/jtag2updi.ino) into the Arduino Nano board. Then wired my board like in this setup.


![](./week15/coppervinyl10.jpg)

So here we can see the $10\mu F$ capacitor for the bloc the rest of the Arduino Nano. And the $4,7k \Omega$ resistor between the UDPI pin of the ATtiny412 and the Arduino's `D6` pin.


![](./week15/ATtiny_x12.jpg)<br/>*source [megaTinyCore](https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/ATtiny_x12.md)*

The idea is to do a first "blink" LED from the ATTiny412. 
I help my self with pinout diagram and choose to connect the LED to the `PA6` as it available for the `TX` step response.

</div>

Once *jtag2updi* and the [*megaTinyCore* are installed](https://github.com/SpenceKonde/megaTinyCore/blob/master/Installation.md) on the Arduino IDE, I should be able to push some code to the ATTiny. But no... I add this error:


```bash
avrdude: jtagmkII_getsync(): sign-on command: status -1
```
I spend 3 hours trying to find the origin of the errors, but I have no idea were it really come from. I know it means the my board is not propelly detected. But even with the [alexandra's assignment](https://fabacademy.org/2020/labs/charlotte/students/alexandra-coston/assignments/week09/) where she faced a similare issue, I could found the solution.

So I decided to refollow another tutorial from [Adam Harris' article from the Blog **SheekGeek](http://sheekgeek.org/2020/adamsheekgeek/attiny412-general-purpose-blinky-board-and-updi-programming).

And there I successfully upload an empty sketch! For me, the solution came from here : ArduinoISP as the programer and the SpenceKonde version of jtag2updi](https://github.com/SpenceKonde/jtag2updi).

So now I'll check if the ATtiny 412 can do anything, so let's try to push a simple LED blink code.

```cpp
#define LED 0

void setup() {
   pinMode(LED,OUTPUT);
   }

void loop() {
   digitalWrite(LED,HIGH);
   delay(100);
   digitalWrite(LED,LOW);
   delay(100);
   }
```

And it's working! I flash my first MicroController without a bootloader. 😃

import blinkAttiny from './week15/blink_attiny.mp4';

<video controls width="80%">
  <source src={blinkAttiny}/>
</video>

Next job is to mount the step response with the conductive fabric and create the code to send the step response throught I2C to another microcontroller. **To be continued...**



## Files
 
Here are the files of the week:

**Plasma**

* [test-dxf.dxf](/files/week15/plasma/test-dxf.dxf)
* [TEST-DXF.CAM](/files/week15/plasma/TEST-DXF.CAM)
* [TEST-DXF.NC](/files/week15/plasma/TEST-DXF.NC)


**Vinyl PCB**

* [step-response-vinyl-simple.dxf](/files/week15/vinyl-pcb/step-response-vinyl-simple.dxf)
* [step-response-vinyl-simple.eps](/files/week15/vinyl-pcb/step-response-vinyl-simple.eps)
* [step-response-Edge_Cuts.dxf](/files/week15/vinyl-pcb/step-response-Edge_Cuts.dxf)
* [step-response-vinyl.dxf](/files/week15/vinyl-pcb/step-response-vinyl.dxf)
* [step-response-vinyl.eps](/files/week15/vinyl-pcb/step-response-vinyl.eps)
* [Blink_ATtiny412.ino](/files/week15/vinyl-pcb/Blink_ATtiny412.ino)
