---
draft: false
sidebar_position: 2
---



# Plans and CAO

## A more basic version

One member of the colletive *Mosquito OS* have design a simpler version of the tricycle. I may use it to design mine.

![](./img/Proto_n1.jpg)

## Joint Types and Assembly

I will need to do some assembly for the project. Has I don't know how to do it in Fusion 360, I started to watch some videos ([this one about joints](https://www.youtube.com/watch?v=Bw08O6XsfDI) and [this one about animation](https://www.youtube.com/watch?v=Y330vJreHIU)).

Here a list of all the joints you can do in Fusion 360 on components together (not bodies).

| Joint Type  | Description                                                                                                                                                                                                                                              | Motion Allowed            |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------|
| Rigid       | A rigid joint fixes two components to one another. It provides no degrees of freedom.                                                                                                                                                                    | None                      |
| Revolute    | A revolute joint has a single rotational degree of freedom, much like a hinge. This joint can rotate around the standard X, Y, or Z axis, or around an edge in the model (a custom axis).                                                                | 1 rotation                |
| Slider      | A slider joint has a single translational degree of freedom. It is used for components that slide along one another. Options are similar to revolute joint options, except that components slide along the selected axis rather than rotating around it. | 1 translation             |
| Cylindrical | A cylindrical joint provides two degrees of freedom: one translational and one rotational. Components joined with a cylindrical joint always rotate around the same axis.                                                                                | 1 translation 1 rotation  |
| Pin Slot    | A pin slot joint also allows two degrees of freedom, but components can rotate around different axes.                                                                                                                                                    | 1 translation 1 rotation  |
| Planar      | A planar joint allows three degrees of freedom. It allows two directions of translation in a plane and a single rotational direction normal to that plane. It is useful for joining two components so they can rotate while sliding across the plane.    | 2 translations 1 rotation |
| Ball        | A ball joint has two degrees of rotational freedom: pitch and yaw. Pitch allows components to rotate around the Z axis. Yaw rotates components around the X axis.                                                                                        | 3 rotations               |
*source [help.autodesk.com](https://help.autodesk.com/view/fusion360/ENU/courses/AP-ASSEMBLIES-AND-JOINTS)*