#include <Wire.h>

void setup() {
  Wire.begin(); // join I2C bus as a master
}

void loop() {
  Wire.beginTransmission(8); // transmit to device address 8
  Wire.write(42); // send the 101010 byte
  Wire.endTransmission(); // stop transmitting
  delay(1); // wait for 1 millisecond
}