import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Weekly Assignments',
    link: 'weekly-assignments/week01',
    Svg: require('@site/static/img/icon-weekly-assignments.svg').default,
    description: (
      <>
	    Each week we are required to complete some assignments. Check 
	    in the documentation section to see the progression.
      </>
    ),
  },
  {
    title: 'Final project',
    Svg: require('@site/static/img/icon-final-project.svg').default,
    link: 'final-project/project-proposal',
    description: (
      <>
	    In parallel, a Final Project is designed to apply the various 
	    skills learned throughout the Fab Academy training. See the documention too.
      </>
    ),
  }
];

function Feature({Svg, link, title, description}) {
  return (
    <div className={clsx('col col--6')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <a href={link}>
	  <h3>{title}</h3>
	</a>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
