void setup() {
  pinMode(D1, OUTPUT); // set digital pin D1 as an output
}

void loop() {
  digitalWrite(D1, HIGH); // set pin D1 high
  delay(1000); // wait for 1 second
  digitalWrite(D1, LOW); // set pin D1 low
  delay(1000); // wait for 1 second
}