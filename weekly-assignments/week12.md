---
title: Week 12 - Molding and Casting
slug: week12
sidebar_position: 12
---


# Molding and Casting


**Group assignment:**

* Review the safety data sheets for each of your molding and casting materials
* Make and compare test casts with each of them

**Individual assignment:**

* Design a mold around the stock and tooling that you'll be using, mill it (rough cut + (at least) three-axis finish cut), and use it to cast parts.

## Casting materials

For the group assignment, I had to have a look at the products I used.

**Machinable Wax**

Machinable Wax is the first material I used. But it's actually made of two products: LDPE plastic and paraffin wax.

<div className="steps">

![](./week12/ldpe_safety.jpg)<br/>*source [borealisgroup.com](https://www.borealisgroup.com/products/catalogue/product-name-ft5230)*

**LPDE Plastic**<br/>I found a [datasheet of a LPDE](https://www.borealisgroup.com/products/catalogue/product-name-ft5230), there are different types of LPDE, but all the datasheets looked similar. They said to be a bit careful with the fume, and the LPDE can easily burn. The melting point is around 100 - 135 °C.

![](./week12/paraffin_safety.jpg)<br/>*source [united-wax.com](http://www.united-wax.com/wd/pi/20161024-120125_2_msds_paraffin_wax_uw.pdf)*

**Paraffin wax**<br/>That can be more dangerous, especially went melted. Spills can go to your skin or eyes. So wearing gloves and glasses is more secure. The melting point is above 50°C, but more important the flash point (where it starts to be ignitable) is 193°C. That is not so far from our 135°C of the melting point of the LDPE plastic. So the mixture needs to be continuously mixed so no part of the mix can have a temperature too high.

![](./week12/data_safety.jpg)<br/>*source [Reschimica](https://www.reschimica.com/gb/silicone-rubbers/122-r-pro-20-liquid-silicone-rubber-for-medium-hardness-molds.html)*

**Silicon**<br/>I read the [datasheet of the R PRO 20 Silicon of Reschimica](https://www.reschimica.com/attachments/en/r-pro-20.pdf). It's a pretty safe product and is considered harmless. Especially for inhalation. <br/>It's a soft (hardness 20 ShoreA) and very elastic silicon<br/>The ratio is $1:1$ (two bases).


![](./week12/platre_safety.jpg)<br/>*source [sader.f](https://sader.fr/produits/platre-modeler)*

**Gypsum Plaster**<br/> I only found the safety data sheet of the product I used in French. But basically it says *This mixture is classified as non-hazardous in accordance with Regulation (CE) No. 1272/2008 [CLP].* The only risk is dust, which can irritate the eyes and the respiratory system. So it's safer to use a mask when using it.

</div>

## DIY machinable Wax

I want to try to mold the grip handle for the trike, it's a piece with some dimensions (more than 13cm). 

<div className="steps">

![](./week12/machinable_wax.jpg)<br/>*source [AliExpress](https://aliexpress.com/item/1005004833826363.html)*

I bought some at the beginning of the Fab Academy machinable wax, but just for a $5cm$x$5cm$ it was more than 35€. So I look [this tutorial](https://www.instructables.com/Machinable-Wax/) and [this one](https://leedshackspace.org.uk/2016/05/09/making-machinable-wax/) to make my machinable wax.

![](./week12/wax_ldpe.jpg)

At the Fablab we recently received a lot of machines and other IKEA pieces of furniture. Each of them was packed with LDPE plastic. So I took some and cut them into pieces.

![](./week12/wax_materials.jpg)

1. water and towler in the event of a fire
2. LDPE flap
3. A mask for the fumes
4. Glass mold
5. Paraffin wax
6. A scale
7. Strainer
8. Wax crayons
9. Ladle
10. Wood Spoon
11. Deep fryer
12. Leather Gloves
13. Safety Glasses
14. Thermometer

![](./week12/wax_ratio.jpg)

The ratio I found in the tutorials and some forums were talking about 4:1 for paraffin wax and LDPE in weight. So first this first test I did $400gr$ of wax and $100gr$ of LDPE.

![](./week12/wax_poring.jpg)

I put the wax in a cheap super small deep fryer. And select first a temperature of 130°C. I used we thermometer to check if the thermostat of the fryer was good, and it was. When the wax was melted I start to add some pieces of LDPE. It was sticky, in the first tutorial it was talking about 300°F (148°C), but I was afraid of plastic fume, so I decided to go up, but more like 140°C.

![](./week12/wax_mold.jpg)

After a *really* long time of pouring the mixture, it started to look ok. So I added some green color with the wax pen. And put the mix in some glass mold pre-heated. But it looked horrible (a lot of air bubbles in it).

![](./week12/wax_mold2.jpg)

In my garage, I have an old oven that I don't use for cooking. I decided to use it to remelt the mixture in the glass mold so the air can escape. after 45min, it looks much better, but we will see after machining it if they are no more bubbles inside.

</div>


## Grip bike handle

The idea is to create a bike handle from rubber. The first iteration will be a simple handle, but if it's working well, I could make another where I can put some space for some pushbuttons for example that could be linked to the board.

As the final result is some silicon rubber, I won't need 2 molds, but just one rigid mold made of DIY machinable wax.

###  3D model 

I will first create the "positive mold", the actual grip handle then use a boolean operation for the actual mold.

<div class="steps">

![](./week12/knurling.jpg)<br/>*source [Wikipedia](https://en.wikipedia.org/wiki/Knurling)*

To have a better grip on the handle I could use a *knurled grip* like in the picture and as explained in this tutorial](https://www.youtube.com/watch?v=9Go-B7-sSL0). But it will be easier to do it with 4 axis CNC machine, not a 3-axis.

![](./week12/fusion1.jpg)

I finally used _crossed_ engraved lines* [like in this video](https://forums.autodesk.com/t5/fusion-360-design-validate/grip-effect/m-p/6872284/highlight/true#M97848). It looks easier to machine, I'll see if I'm right. 


![](./week12/fusion2.jpg)

So here is the mold (just a subtraction of half of the handle). As expected I'll have a problem with the coil patterns that are perpendicular to the Z-axis. Also, I need to look at what I'll do for the center. I may just use the handlebar directly.

![](./week12/fusion3.jpg)

So I decided to put the *grip* pattern only a the end of the handle. So I went back on the history in Fusion360 to *cut* the middle part.

![](./week12/fusion4.jpg)

It's still not perfect, and I also so the edge of the cylinder that I won't be able to remove with my 6mm end ball mill.

![](./week12/fusion5.jpg)

So I changed the tactic. As I can only work with tools that come from a vertical Z-axis, I need the grip to be formed like that.

![](./week12/fusion6.jpg)

Once done, I also need to add some chamfer edges as my bits with the ball-end will not fit in all the edges. To the cylinder, I added a $3mm$ radius chamfer for the $6mm$ diameter ball end mill. And a radius of $0,75mm$ for my $1,5mm$ bit.

![](./week12/fusion7.jpg)

Now I need the second part of the mold and a registered perimeter to fix the two parts together. I was careful to add some curves to the perimeter to be sure both parts can be milled with a $6mm$ flat-end mill.

![](./week12/fusion8.jpg)

So now I need a funnel form for the *silicon feed* (inlet). I've put it at the top of the first part of the mold. And just at the edge to be able to trim off easily.

![](./week12/fusion9.jpg)

I also made a section analysis to be sure the silicone will be able to pass in all the space between the two molds and the handlebar.

</div>

### Toolpath

<div className="steps">


![](./week12/camfusion1.jpg)

For the $6mm$ bit, I used 3000rpm spindle speed and a feed rate at 2400mm/min for the 1.5 10000rpm and 3000mm/min inspired by this week's assignment from Jules Topart](http://fabacademy.org/2021/labs/lamachinerie/students//assignments/12_molding/#cnc-machined-mold)

![](./week12/camfusion2.jpg)

When selecting the boundary for the milling, I choose the center of the boundary as the limit, so the tool goes to the border of the wax.

![](./week12/camfusion3.jpg)

I created 2 folders with the two different bits, so I can generate the G-code with multiple toolpaths with the same tool.

![](./week12/camfusion4.jpg)

Then I did the same for the second part of the mold.

</div>

### Machining the two mold

<div className="steps">

![](./week12/cambam_surface.jpg)

As my wax is far from straight I had to surface it first. So I create a quick G-code in CamBam.

![](./week12/wax_surface.jpg)

A bit hard to fix, but as it's a bit malleable, I could screw it. Then I surface it. I can see that my Z-axis needs to be reset correctly.

![](./week12/wax_surface2.jpg)

Then I did the other face.

![](./week12/mold_pocket.jpg)

Then I launch directly the adaptive pocket toolpath and the flat surface keeping the flat-end mill of $6mm$. The Z-zero is super easier to set and super accurate because I just surface it and kept the previous Z minus the depth of the surface.

![](./week12/mold_final_result.jpg)

Then I change the tool for the $1,5mm$ ball-end bit and launch the finish toolpath. It looks pretty ok. At least all the parts fit!

</div>

### Silicon

<div className="steps">


![](./week12/density.jpg)

In Fusion, you can change the type of material of your body. So I changed it to Silicon (Modify > Physical material > Silicon, Rubber). Then A was able to get the weight I needed to pour in the mold (it's just one part so I need the double + a bit more). I decided to go for 60gr.

![](./week12/mold_funnel.jpg)

I fix the 3 parts together (Part A, B and the Handlebar). Then I create a basic funnel.

![](./week12/mold_outler.jpg)

I poured the silicon till I started to see some silicon coming from the *outlet*. I add a lot of difficulties to pouring the silicon into it. It was too sticky. I should have done a bigger *inlet*. We will see if the silicon was able to reach all the parts of the mold.

![](./week12/grip1.jpg)

And here we go. After 24 hours (I work on Tuesday so I didn't touch it for the day). We can see the outlet and the inlet. The result is pretty clean, I was afraid that the silicon wouldn't have been able to reach all the mold, but I was wrong.

![](./week12/grip2.jpg)

We can still see one bubble that was a the top between the vent and the pouring funnel.

![](./week12/grip3.jpg)

We can also see the register perimeter, but it's ok. The overall looks fine. But that rubber silicon is not made for a grip handle. It's still super sticky as a tactile sensation.

</div>

## 3 steps molding and casting

To try the 3 steps molding and casting process, I will do a simple form made from gypsum plaster. As it's a rigid material, I won't be able to pour it directly in the machinable wax, the risk is to break the piece while trying to remove it from the mold. So the idea is to create a first mold in wax and then from it a mold in silicon to cast the gypsum.

### 3D modeling

<div className="steps">

![](./week12/3steps1.jpg) 

So I will create a small "bolt" style ring. To be honest, it's just to test the process, it won't be useful. In the 2D design, I directly add the inlet and outlet for the plaster. Knowing that I want to use only one tool in the CNC (a flat endmill of 2mm), I already add some *fillets* with a radius of $1mm$ to be able to see the result that will be made by the tool. And also be able to *design* with the tool constraints. I also add the registration perimeter for the two parts of the silicon mold.

![](./week12/3steps2.jpg) 

Then from that sketch, I can create the bodies in the reverse process it will be made. So first I created the _bolt_ ring* and its inlet and outlet.

![](./week12/3steps3.jpg)

Then the two silicon molds and their register perimeter, then I did a boolean operation to remove the *bolt ring* from the silicon.

![](./week12/3steps4.jpg)

I moved the two silicon molds on the same plane.

![](./week12/3steps5.jpg)

Then I create a body that will represent my machinable wax and remove the two silicons molds from it.

![](./week12/3steps6.jpg)

Then I create the toolpath. As I already did a 3-axis milling process for the handle bike, here I kept it super simple. With only one tool and two pocket operations (one rough and one finishing), I could generate the GCode to mill the first mold.


</div>

### Molding and casting

<div className="steps">

![](./week12/3steps_pic1.jpg)

After only 25 minutes it was done!

![](./week12/3steps_pic2.jpg)

The result it's pretty clean, I should have, maybe, cleaned the design for the pouring and vent access, but for the process, it'll be enough.

import vibrateSilicon from './week12/vibrate_silicon.mp4';

<video controls width="100%">
  <source src={vibrateSilicon}/>
</video>

Once the components A and B of the silicon are mixed (it's the same silicon as the handle bike), I use my phone in vibration mode to remove the bubble (I called myself with Skype =).

![](./week12/3steps_pic3.jpg)

The first try was horrible. With such a small quantity, and my kitchen weighing scale is hard to properly respect the ratio 1:1.

![](./week12/3steps_pic5.jpg)

I used some White spirit to clean the mold (wearing gloves and a mask). But it damaged a bit the plastic.

![](./week12/3steps_pic4.jpg)

The second test was better, but still sticky. But let's try this.

![](./week12/3steps_pic6.jpg)

So for the gypsum plaster, the ratio is 1:1 with water.

![](./week12/3steps_pic7.jpg)

I use some tape to seal the molds together. And I pour it from the inlet.

![](./week12/3steps_pic8.jpg)

The result is not good. The gypsum didn't go on the top of the mold. The vent should have been put at the really top.

![](./week12/3steps_pic9.jpg)

I redid another silicon mold, with a bit more of the "base A", the component that has "the hardness". It was way less sticky and the corners were sharper, but they were more bubbles in it.

![](./week12/3steps_pic10.jpg)

So it did more tries. It was a bit better. But far from perfect.


![](./week12/3steps_pic11.jpg)

We can see the registration marks.

</div>

### Room for some improvements

I'm glad that I tried this process. I've learned a lot but here are the things I'll try to do next time:

* A Better pouring and vent location
* Add Some ramp to the walls so it's easier to remove
* Try maybe a better silicon
* Try a vacuum for the bubbles


## Files

Here are the files of the week:

* [grip_handle.f3d](/files/week12/grip_handle.f3d)
* [A_ball.nc](/files/week12/A_ball.nc)
* [A_flat6.nc](/files/week12/A_flat6.nc)
* [B_ball.nc](/files/week12/B_ball.nc)
* [B_flat.nc](/files/week12/B_flat.nc)
* [3steps_casting.f3d](/files/week12/3steps_casting.f3d)
* [boltmold.nc](/files/week12/boltmold.nc)
