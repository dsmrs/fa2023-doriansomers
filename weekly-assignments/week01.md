---
title: Week 1 -  principles and practices / project management
slug: week01
sidebar_position: 1
---

#  Principles and practices / Project management


## Principles and practices

* Plan and sketch a potential final project
* Sketched your final project idea.
* Described briefly what it will do and who will use it.

My proposition for the final project is to make an electrical trike, **more info in** [**the final project documentation**](../final-project/project-proposal)

## Project management

* Setup a project management environment
* Use git to create a website published on Gitlab
* Create the following pages:
    * Weekly Assignments
    * Final Project
    * "About me"

## Spiral Goals

* [x] Set up a nice mobile project management environment (calendar, Kanban, multiples screens, Linux, etc.) (**Assignment**)
* [x] Push a website with both documentation of the weeks, a page about me, and the assignments signed (on the Gitlab repository) (**Assignment**)
* [x] Compress the Images with ImageMagick (**Assignment**)
* [x] Transfer the website to a Docusaurus website (**Bonus**)
* [x] Create a double documentation (**Bonus**)
* [ ] Create a pipeline when adding an image to compress it with ImageMagick (**Bonus**)
* [ ] Change the theme to make it a bit prettier (**Bonus**)
* [ ] Link Outline or HedgeDoc documents to the git repo's markdowns (**Bonus**)

### Kanban

To be able to correctly do those 20 weeks of training, I need to set it up properly. Following both Neil Gershenfeld recommendation and some for the student's Bootcamp, I decide to be able to **work in parallel on the assignment**. I use [Kanban board](https://en.wikipedia.org/wiki/Kanban_board) as a simple task management tool (To Do, Doing, Done). It's a tool I used a lot, working as a Web Developer in Startups in my previous life. I've decided to use a physical one (see picture) as I'm more focused on paper than on some [Trello](https://trello.com/)-like apps. The task categories are on *post-it* too so they can evolve during the program.

![kanban](img/week01/kanban.jpg)

### Spiral goals

To challenge myself, I'll add some [Spiral goals](#spiral-goals) a the beginning of each week (see above). The first levels are just the **assignment done right**, and the next ones are **propositions to go further on the topic**. It's ok to not complete all of them. And I still can come back later on it if I have time. 

### Calendar

To be able to a **whole look at the next 20 weeks**, I've also drawn a big paper/cardboard calendar. It's foldable like the Kanban board so I can take them with me at the Fablab or at Home.

![calendar](img/week01/calendar.jpg)

### Notebook

I use the bullet journal method to manage my day-to-day life for 3 years now. For the Fab Academy, a started a special notebook, to **quickly write down thoughts, ideas and, tasks**.

![notebook](img/week01/notebook.jpg)

### Development Stack

To code, I know that **Linux is much more convenient**. So first I tried to install a [lite version of Zorin OS](https://zorin.com/os/download/15/lite/32/) on an old HP laptop, but after installing the OS, git, vim and starting to work on my website, I realized that the latest versions of [Node.js](https://nodejs.org/en/) weren't compatible with 32bit systems. I have another HP Pavillon DV7 that is 64-bit, but it needs a new CPU fan. So in the meantime, I've started to work on my MSI steelseries on [Windows Subsystem for Linux (WSL)](https://learn.microsoft.com/en-us/windows/wsl/install). I've already used it a bit, so [**git**](https://git-scm.com/), [**tmux**](https://github.com/tmux/tmux/wiki), and [**vim**](https://www.vim.org/) were already all configured. And as I maintain a [Yunohost webserver](https://yunohost.org/#/index_en) at my [Fablab](https://lafabriquecaylus.fr), I've already an [RSA ssh key](https://docs.gitlab.com/ee/user/ssh.html). I just needed to add it to my [gitlab's fabcloud profile preference](https://gitlab.fabcloud.org/-/profile/keys).

To get your RSA-key to add to your Gitlab profile, you can just write this on your Linux terminal:

```bash
cat .ssh/id_rsa.pub
```

**Git Tutorial**

When I discovered GIT, 8 years ago, I've watched this [good video tutorial](https://www.youtube.com/watch?v=V6Zo68uQPqE) (in French), the code school that did this have also [an English version of it](https://www.youtube.com/watch?v=Z9fIBT2NBGY) and even [a Spanish one](https://www.youtube.com/watch?v=xZUAzSukXFs). I recommend those tutorials as they explain how to connect through  SSH your local `git` to Github (and it's similar to linking it to Gitlab).

[On the *About me* page](https://fabacademy.org/2023/labs/kamplintfort/students/dorian-somers/about-me#Tools-I-use) **I will add, during the training, all the different tools I will use** for the assignment, the final project and to document all of that.

### Website

Why I needed a late version of Node.js? Because I wanted to use [**Docusaurus**](https://docusaurus.io/) **as a doc website static builder**. There I can use flavored markdown.

:::info
With *Admonitions* syntax like this
:::

Or [MDX](https://mdxjs.com/) plugins (markdown on steroids) or [Latex](https://www.latex-project.org/) formulas like this: $I = \int_0^{2\pi} \sin(x)\,dx$

To manage my Node.js I use [Node Version Manager(nvm)](https://github.com/nvm-sh/nvm). I install the latest [LTS version](https://en.wikipedia.org/wiki/Long-term_support) (v18.13).

:::success
Use **nvm** to manage **Node.js**, and use **Node.js** to manage [**yarn**](https://yarnpkg.com/) package manager using [corepack](https://yarnpkg.com/getting-started/install). If to have already installed yarn with another tool (ex: `sudo apt install`), as I did, you can find the *bins* executable with the Linux's command `whereis yarn` and remove those folders to have a clean install.
:::

In my `code/` folder, I've cloned my Fab Academy repository like this: 

```
git clone git@gitlab.fabcloud.org:academany/fabacademy/2023/labs/kamplintfort/students/dorian-somers.git` using the SSH clone link.
```

In parallel I started a bootstrap Docusaurus template using [this command](https://docusaurus.io/docs/installation#scaffold-project-website):

```bash
npx create-docusaurus@latest dorian-somers classic
```

I've removed the `public/` folder on the Fabacademy repo and copied all the files from the bootstrapped folder.

```bash
cd ~/code/dorian-somers && rm -R public/ && cp ../tmp/dorians-somers/* .
```

Then I installed the dependencies and run the Docusaurus development server.

```bash
yarn install && yarn start
```

Here is the **organization of the repository**:

```
dorian-somers/
├── docs/
│   ├── weekly-assignements/
│   │   ├── img/
│   │   │   └── *.jpg           # Images for the assignments
│   │   ├── week01.md           # Markdown files for the assignments
│   │   ├── ...
│   │   └── week17.md
│   ├── final-project
│   │   ├── img/
│   │   │   └── *.jpg           # Images for the final project
│   │   ├── project-proposal.md # Markdown files for the final project
│   │   ├── ...
│   │   └── conclusion.md
├── blog
│   ├── 2023-01-27-welcome.md   # Markdown files for the "Journal"
│   ├── ...
│   └── 2023-06-20-the-end.md
├── src
│   └── pages
│       └── about-me.md         # Other web pages
├── static
│   └── img
│       └── *.jpg               # Images for the other pages
├── .gitignore                  # Ignore files that don't need to be pushed
├── .gitlab-ci                  # Command to publish the website
├── README.md                   # Readme of the repo
├── agreement.md                # Student's agreement signed
├── ...                         # Some config files (lock, ...)
├── docusaurus.config.js        # Configuration file for Docusaurus (title, navbar, ...)
└── sidebars.js                 # Configuration of the documentation's sidebar 
```

I've created the first markdown file (week01.md, about-me.md, and a blog post) with [vim](https://www.vim.org/). 

I've changed the `.gitlab-ci` file inspired [by this file](https://gitlab.com/pages/docusaurus/-/blob/master/website/.gitlab-ci.yml). I've just used a more updated [Node.js alpine docker image](https://hub.docker.com/_/node) and changed `master` branch to`main` branch.

### Markdown editor

Vim is nice, but having a nice preview of your markdown can be convenient. On the Yunohost server of my Fablab, I have a [**HedgeDoc**](https://hedgedoc.org/) instance. There I can create a flavored markdown file and see a real-time preview. It uses a really similar markdown as Docusaurus, and when it doesn't I can still edit the syntax on Docusaurus using the [MDX plugin](https://docusaurus.io/docs/markdown-features/plugins) mechanism. The next step would be to link both HedgeDoc document and the markdown file on the repo with the [HedgeDoc Command line](https://github.com/hedgedoc/cli).


*09/02/2023 Update* 

### Split the documentation in two.

Docusaurus by default handle one documentation. If you want 2 of them [you need to reconfigure it](https://docusaurus.io/docs/docs-multi-instance). I moved the `weekly-assignments/` and the `final-project/` folder to the root of the repository and changed the `config.presets` and `config.plugins` as follow in the `docusaurus.config.js` file :

```javascript
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          path: 'weekly-assignments',
          routeBasePath: 'weekly-assignments',
          editUrl:
            'https://gitlab.fabcloud.org/academany/fabacademy/2023/labs/kamplintfort/students/dorian-somers/-/tree/main/',
        },
      },
    ],
  ],
  plugins: [
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'final-project',
        path: 'final-project',
        routeBasePath: 'final-project',
        sidebarPath: require.resolve('./sidebarsFinalProject.js'),
        editUrl:
            'https://gitlab.fabcloud.org/academany/fabacademy/2023/labs/kamplintfort/students/dorian-somers/-/tree/main/'
      },
    ],
  ],
};

```

**Update 17/02/2023**

### Add some Charts

Using [React Chart.js Component](https://react-chartjs-2.js.org/) I've been able to add a nice chart in the [week 4 assignment for the Benchmark](./week04.md#some-banchmarks).

All I had to do is to install the following dependencies.

```bash
yarn add chart.js && react-chartjs-2
```

In Docusaurus, I can add directly some react in the markdown file, I added some JS code looking at [this example](https://react-chartjs-2.js.org/examples/vertical-bar-chart) in their doc.*


**Update 14/04/2023**

### Add the two columns' layout

As said in a blog post, I used ChatGPT to add some CSS functionality to make my documentation more compact. I'll refactor the website week by week.