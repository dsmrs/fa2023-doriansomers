
const int redPin = 1; // Declare the pin number for thr RBG LED
const int greenPin = 2;
const int bluePin = 3;


int delayVal = 1000;


// Insert the colors values in the map
const byte BLACK = 0b000;
const byte RED = 0b100;
const byte GREEN = 0b010;
const byte BLUE = 0b001;
const byte MAGENTA = 0b101;
const byte CYAN = 0b011;
const byte YELLOW = 0b110;
const byte WHITE = 0b111;

// Input variable 
String command;

// store the current color
byte currentColor;


// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  Serial.begin(9600);
  delay(1000);
  Serial.println("Type the color");
  currentColor = BLACK; // set currentColor to Black
}

// the loop function runs over and over again forever
void loop() {
  //displayColor(MAGENTA); //Deisplay a color on the RGB LED
   if(Serial.available()){  // returns the bytes of data which have arrived in the serial buffer
       command = Serial.readStringUntil('\n'); // combine all the characters in the sent message into a single Arduino string until the /n charactec (end of the line).
       Serial.print("You typed: " );
       Serial.println(command);
       commandToColorByte(command);
       
   }
   displayColor(currentColor);
  //Serial.println(currentColor);
}


void displayColor(byte color) {
  // Set the appropriate bit from the byte
  digitalWrite(redPin, bitRead(color, 2));
  digitalWrite(greenPin, bitRead(color, 1));
  digitalWrite(bluePin, bitRead(color, 0));
}

// function that return the right color byte
void commandToColorByte(String command) {

  if(command.equals("black")){
    currentColor = BLACK;
  }
  else if(command.equals("red")){
    currentColor = RED;
  }
  else if(command.equals("green")){
    currentColor = GREEN;
  }
  else if(command.equals("blue")){
    currentColor = BLUE;
  }
  else if(command.equals("magenta")){
    currentColor = MAGENTA;
  }
  else if(command.equals("cyan")){
    currentColor = CYAN;
  }
  else if(command.equals("yellow")){
    currentColor = YELLOW;
  }
  else if(command.equals("white")){
    currentColor = WHITE;
  }
  else{
      Serial.println("Invalid command");
      Serial.println("You need to choose between those colors:");
      Serial.println("black");
      Serial.println("red");
      Serial.println("green");
      Serial.println("blue");
      Serial.println("magenta");
      Serial.println("cyan");
      Serial.println("yellow");
      Serial.println("white");
  }
}
