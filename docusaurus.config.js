// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');
const math = require('remark-math');
const katex = require('rehype-katex');
const emoji = import('remark-emoji');


/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Fab Academy 2023 - Dorian Somers',
  tagline: '20 weeks to make (almost) anything',
  favicon: 'img/favicon.ico',

  // Set the production url of your site here
  url: 'https://fabacademy.org/',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/fa2023-doriansomers/',


  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },
  markdown: {
    mermaid: true,
  },
  themes: ['@docusaurus/theme-mermaid'],

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          path: 'weekly-assignments',
          routeBasePath: 'weekly-assignments',
          remarkPlugins: [math, emoji],
          rehypePlugins: [katex],
          editUrl:
            'https://gitlab.fabcloud.org/academany/fabacademy/2023/labs/kamplintfort/students/dorian-somers/-/tree/main/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.fabcloud.org/academany/fabacademy/2023/labs/kamplintfort/students/dorian-somers/-/tree/main/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  plugins: [
    'docusaurus-plugin-image-zoom',
    ['docusaurus-csv-to-table', { esModule: true }],
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'final-project',
        path: 'final-project',
        routeBasePath: 'final-project',
        remarkPlugins: [math, emoji],
        rehypePlugins: [katex],
        sidebarPath: require.resolve('./sidebarsFinalProject.js'),
        editUrl:
            'https://gitlab.fabcloud.org/academany/fabacademy/2023/labs/kamplintfort/students/dorian-somers/-/tree/main/'
      },
    ],
  ],
  stylesheets: [
    {
      href: 'https://cdn.jsdelivr.net/npm/katex@0.13.24/dist/katex.min.css',
      type: 'text/css',
      integrity:
        'sha384-odtC+0UGzzFL/6PNoE8rX/SPcQDXBJ+uRepguP4QkPCm2LBxH3FA3y+fKSiJ+AmM',
      crossorigin: 'anonymous',
    },
  ],
  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/social-media.jpg',
      docs: {
        sidebar: {
          hideable: true,
        },
      },
      navbar: {
        title: 'Dorian Somers',
        items: [
          {
            type: 'doc',
            docId: 'week01',
            position: 'left',
            label: 'Weekly Assignments',
          }, 
          {to: '/final-project/project-proposal', label: 'Final Project', position: 'left'},
          {to: '/blog', label: 'Journal', position: 'left'},
	  {to: '/about-me', label: 'About me', position: 'left'},
          {
	    href: 'https://gitlab.fabcloud.org/academany/fabacademy/2023/labs/kamplintfort/students/dorian-somers',
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Weekly Assignments',
                to: '/weekly-assignments/week01',
              },
              {
                label: 'Final Project',
                to: '/final-project/project-proposal',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Journal',
                to: '/blog',
              },
              {
                label: 'GitLab',
                href: 'https://gitlab.fabcloud.org/academany/fabacademy/2023/labs/kamplintfort/students/dorian-somers',
              },
            ],
          },
        ],
        copyright: `CC-BY-SA ${new Date().getFullYear()}. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
      zoom: {
        selector: '.markdown :not(em) > img',
        config: {
          // options you can specify via https://github.com/francoischalifour/medium-zoom#usage
          background: {
            light: 'rgb(255, 255, 255)',
            dark: 'rgb(50, 50, 50)'
          }
        }
      },
    }),
};

module.exports = config;
