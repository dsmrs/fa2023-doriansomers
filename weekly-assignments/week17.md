---
title: Week 17 - Invention, Intellectual Property and Business Models
slug: week17
sidebar_position: 17
---

# Invention, Intellectual Property and Business Models

* Develop a plan for dissemination of your final project.
* Prepare drafts of your summary slide (presentation.png, 1920x1080) and video clip (presentation.mp4, 1080p HTML5, < ~minute, < ~10 MB) and put them in your website's root directory


## Slide and video

Here is the slide where I show all the technics used.

![](../static/presentation.png)


For the structure of the video, I did the following:

1. concept the problem
2. different process
    1. Modeling CAD
    2. Plasma
    2. CNC
    3. 3D print
    4. Coding
    5. Failures (3D prints)
3. The solution (on battery + range test)

And here is the result:

<video controls width="100%">
  <source src="/2023/labs/kamplintfort/students/dorian-somers/presentation.mp4"/>
</video>

## Intellectual Property

I have a strong belief in Opensource. If an invention is good for humanity, I'd prefer to give it to the commons than keep it for myself.

For all the Hardware parts I decided to use the **CERN Opensource Hardware Licence**: [CERN-OHL-W (weakly reciprocal)](https://ohwr.org/cern_ohl_w_v2.txt). It's trying to be the GPL for hardware, just the weakly reciprocal, letting you use components from the market (like: bolt, screws, basic components) that are not opensource.

For the software, I use the **GNU General Public License** [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html). I like opensource project to stay in the opensource world.

For the texts and the image, I use the **Creative Commons Attribution-ShareAlike** copyright [CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/).

## Opportunities and Business Model

What a fun project! But could it be useful?

### LoRaWAN

I don't think the idea to buy a small LoRa module to be able to light up some traffic signs will ever work. But what could work it's removing the need for any module by using the LoRaWAN network.

The idea will be that the person on his bike will launch an App on his smartphone that will look for his destination and will send through the LoRaWAN network the order to the right traffic sign to light up.

The traffic sign could light up with a different probability for the next car to cross the bicycle. Such as if there is a junction between the car and the bike, the car could turn and never see the bike. Something like:

* 100% sure = Red
* 99%-10% = Orange to yellow
* less then 10% = Green

That's just an idea, the green color is probably a bad one actually because the car could cross any bike that is not connected to the "Signs network".

![](./opportunities.jpg)

### Cost saving politics

A French non-profit organization about bicycles has made a good guide about the cost of "bike policies"](https://www.corse.developpement-durable.gouv.fr/IMG/pdf/guide-couts-politiques-velos-cvtc.pdf) (in French of course). There I could read that the cost for cycle lanes ranges from 2-4€/meter (just a white line on the road w or /w resin) to more than 400€/meter for private bike roads.

As an example, I'll choose the road between two small towns where I live](https://goo.gl/maps/rq9Wym2HE5srG4fu7). The distance between them is 11,6km.

So to adapt this road for a safer ride for the bikes (there is nothing right now), it will cost, for the very minimal cycle lane, **more than 23200€**.

If we count only the junction when we have left the towns, there are only six main junctions along the 11,6km of the road. So if we use the traffic sign, for the best coverage (but is not needed), 7 traffic signs will be needed. So it means $\frac{23200}{7}=3314,29$ that could be something to ask for each sign. Of course, the real cost should be calculated to compare.

### Market

The best is to propose this solution as a temporary solution (nothing better than having a proper cycle lane). To find some places to experiment, [this Newspaper has made a map of the most dangerous road by bike in France](https://www.leparisien.fr/societe/cyclisme-la-carte-des-zones-les-plus-dangereuses-a-velo-15-02-2021-8424968.php) or there is another Bicycle organization that has made a [cooperative tool where any cyclist can tell what is the road condition](https://on3v.veremes.net/signalement/dashboard/carte_signalements). It's a good start.

### Next step

While looking for any LoRaWAN Gateway, I discover that [there is actually one nearby!](https://ttnmapper.org/heatmap/gateway/?gateway=lorawan-ttn-courtes&network=NS_TTS_V3://ttn@000013). And it's from a member of the Fablab =) I have a meeting with him at the end of the month so he can help me with that.

I also have a contact in the French Government Agency for Ecological Transition, maybe he could help my do get some money to do some R&D. To be continued...