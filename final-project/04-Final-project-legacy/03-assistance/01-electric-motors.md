---
draft: true
sidebar_position: 1
---

# Electric motors

Freewheel Geared Motor hub
Direct Drive motor hub


## My material

### Brushless motor

![](./img/N5065-140kv-motor.jpg)

### Controller

![](./img/FESC6-6.jpg)



### Links

https://blog.ozo-electric.com/2020/04/21/moteur-brushless-velo-electrique-conception/

https://www.gadgetreview.com/direct-drive-vs-geared-motor-electric-bike#:~:text=A%20direct%20drive%20motor%20offers,power%2C%20but%20are%20far%20noisier.

https://electricbikereport.com/electric-bike-direct-drive-geared-hub-motors/

https://www.youtube.com/watch?v=8NiQ_zjYJVA