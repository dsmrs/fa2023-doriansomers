---
title: About Me
---

# About Me

![me](/img/dorian-drawing.jpg)

Hi! I'm Dorian, a Belgian living in France for 6 years. 👋

With a Civil engineering background, I tried different jobs. I've been a web developer for almost 4 years (AngularJS and RubyOnRails), an Instructor at a coding bootcamp ([LeWagon](https://www.lewagon.com/)) and a substitute teacher at High Schools in Math, Physics, and Chemistry.

It has been a while since I have the idea to try the experience of the Fab Academy and Fablabs in general. To discover this world, I did an internship at the Fablab Barcelona in 2018, where I saw the Fab Academy students and their projects.

Since then I've started to work as a Fab manager at [La Fabrique Caylus](https://lafabriquecaylus.fr) (formerly *Fablab Origami*) in the South of France. So I already know the basics of most of the machines present in the Fablab. But they are so much to know, I still have a lot to learn, life is a "never stop" learning adventure.

Embedded programming and all the electronic part are gonna be the biggest challenge for me. I'm looking forward to seeing what I'll be capable of with the help of the program, the teacher, the instructors, the other students, and Fablab's community in general.

## Tools I use

Here you'll find all the tools I used for the Fab Academy 2023.

### Computers

* MSI Steelseries on Windows 10 with Ubuntu WSL
* HP Pavillon Pv7 with Pop OS
* Lenovo WorkStation P620 (with a RTX A4000)

### Software/applications

All the basic tools from Pop OS and Windows 10.

**web development**

* git
* VS Code
    * Some Markdown previewers
    * Grammarly extension
    * PlateformIO extension
* Docusaurus as Doc builder (in ReactJS)
* Gitlab instance from the FabAcademy to serve the website

**Utilities**

* ImageMagick
* FFMpeg
* OBS Studio
* HedgeDoc
* Outline

**Design/CAO/CAM**

* Inkscape
* LibreCAD
* FreeCAD
* Fusion 360
* Gimp
* KidCAD
* CURA
* Prontface
* OpenSCAD


**Machines**

* Arketype Jade 6090 - Laser C02
* Graphtec CE6000 - Plotter
* Ultimaker 2 Extended+ - 3D printer
* Vertex K8400  - 3D printer
